import numpy as np
import pickle as pkl
from argparse import Namespace

###########
## Paths ##
###########

paths=Namespace()
## Path to PyRADISE and PySSD
paths.ToPackage ='/home/sondrevf/Documents/12_CERN_EPFL/22_Python&Ipython/01_Ipython_work/PyRADISEgit'

## Desired paths where to store pickled output data and plots
paths.ToStorage = '/home/sondrevf/Documents/12_CERN_EPFL/22_Python&Ipython/01_Ipython_work/01_DistributionDiffusion/'
paths.ToPickles = paths.ToStorage+'01_Output/'
paths.ToPlots   = paths.ToStorage+'03_Plots/'

## Path to files containing details about wakefield modes
paths.ToWmodeFiles='/home/sondrevf/Documents/12_CERN_EPFL/22_Python&Ipython/01_Ipython_work/06_LHCDipoleEta/01_Output/fromXB/'

########################################
####### Configuration parameters #######
########################################

s= Namespace()
s.flag_Radial = 1               # true: radial PDE, false: action PDE
s.flag_ICPoint = 0              # IC given as point value or average for cell
s.solve_method=['RK45','RK23','Radau','BDF','LSODA'][3] # Use BDF, if not, it is numerically unstable
s.flag_AdjustTmax  = 0          # Calculate approximately how long the latency will be to set tmax
s.flag_UpdateCoeff = 1          # 1 (0): Recalculate Diffusion coeffiecent every step
s.flag_UpdateCoeffAllTheWay = 1 # If False (and flag_UpdateCoeff is True): Do not update coefficients the last X% of the drilling (see Solver.py)
s.flag_SDRemoveLoops = 0        # If True (and flag_UpdateCoeff is True): Remove loops of SD to calculate the diffusion coefficient
s.relInstabilityThreshold = 0.0 # (If flag_UpdateCoeff is True): Consider instability reached when relInstabilityThreshold*100% remains of the drilling
s.flag_ProjX = 0                # true: Calculate projection on x and y axis (time consuming and rarely interesting)
s.xMax_ProjX = 3                # Maximum position amplitude to project to
s.flag_AdjustGrid = 0           # true: shift grid so that one cell is centered on the avg. Interesting in 1D with iCoeff=2
s.maxCPU = 1                    # Maximum number of CPUs to use for SD calculation
s.BC_xMax = 0                   # i = [Dirichlet Psi=0, Neumann djPsi=0, Robin: djPsi+Psi=0 or drPsi+rPsi=0, dPsi const]
s.BC_xVal = 0                   # Value of BC at xMax
s.BC_yMax = s.BC_xMax
s.BC_yVal = s.BC_xVal
s.debug   = 1                   # If>0: Print additional information to user


## Define time steps
s.time_scale=1                  # To get prefactor on time, [0.001,1,60,3600,'auto']
s.f_rev = 11.2455e3             # Revolution frequency (relevant since noise is given in amplitude per turn)
s.tmax = 1e4                    # Max number of seconds to iterate distribution for
s.nturns = s.tmax/s.f_rev       # Max number of turns ----------------------------
s.ntODE = 201                    # Number of times to solve the distribution for, overruled if flag_AdjustTmax=True
s.tsODE = np.linspace(0,s.tmax,s.ntODE)
s.ntPSImax = 50                 # Maximum number of times to save the distribution for


## Define grid
s.ND = 2                        # Number of dimensions, 1 or 2
s.Ncx = 1000                    # Number of cells in horizontal action space
s.Ncy = 1000                    # Number of cells in vertical action space
s.JxMax = 20                    # Max horizontal action in the PDE
s.JyMax = s.JxMax               # Max vertical action in the PDE
s.rxMax = np.sqrt(2*s.JxMax)    # Max horizontal phase-space-radius
s.ryMax = np.sqrt(2*s.JyMax)    # Max vertical phase-space-radius
s.JMaxSD = [18, s.JxMax][1]     # Max action to integrate over with PySSD

s.interpOrderPsi = 1            # Interpolation order of the distribution


#######################################
## Default Beam & Machine parameters ##
s.Qx = 0.31                     # Horizontal fractional tune
s.Qy = 0.32                     # Vertical fractional tune
s.Qs = 0.00191                  # Synchrotron tune
s.sigma_dpp = 1e-4              # rms momentum spread
s.Qpx= 15                       # Horizontal linear chromaticity Q'_x
s.Qpy= 0                        # Vertical linear chromaticity Q'_y
s.gx = 0.02                     # Horizontal feedback gain, damping time = 2/gx
s.gy = s.gx                     # Vertical feedback gain, damping time = 2/gx
s.ax =  2.45e-5 * 1.5           # Horizontal normalized in-plane detuning coefficient
s.bx = -0.7*s.ax                # Horizontal normalized cross-plane detuning coefficient
s.ay = s.ax                     # Vertical normalized in-plane detuning coefficient
s.by = s.bx                     # Horizontal normalized cross-plane detuning coefficient


## Physics parameters
s.iCoeff = 5                    # Which diffusion coefficient [0, IBS, IBS + LL (r),NEW, NEW+LL ...,[10],constant (J),K*LL(J)]
                                # iCoeff=1: Intra-beam scattering (IBS)
                                # iCoeff=2: IBS + Feedback affected diffusion (FB)
                                # iCoeff=3: IBS + Wakefield driven diffusion (W-USHO)
                                # iCoeff=4: IBS + FB + W-USHO
                                # iCoeff=5: IBS + Wakefield driven diffusion (W-DI)
                                # iCoeff=6: IBS + FB + W-DI ... NOT IMPLEMENTED, because W-DI actually includes FB if using modes from BimBim

## iCoeff 2: Feedback affected diffusion parameters
s.damperAlpha = 1                       # damperAlpha = 0: includes drift, damperAlpha=1: cancel drift term
s.JAvg  = 1                             # Average action of a beam. 1 for exponential beam
s.dQxAvg=(s.ax+s.bx*(s.ND>1))*s.JAvg    # Average Horizontal tune
s.dQyAvg=(s.ay+s.by*(s.ND>1))*s.JAvg    # Average Vertical tune

## iCoeff 3,5: Noise Excited Wakefield parameters
s.wmodeQ0x=[]   ; s.wmode__DQx=[]   ; s.wmodeMom0x=[]  ;  s.wmodeMom1x=[]    # Initialized to empty
s.wmodeQ0y=[]   ; s.wmode__DQy=[]   ; s.wmodeMom0y=[]  ;  s.wmodeMom1y=[]    # Initialized to empty
# s.wmodeQ0x   = [s.Qx,s.Qx] ; s.wmode__DQx = [-1.47e-4 + 1.25e-5j,1e-4+1.5e-5j]  ; s.wmodeMom0x = [1,1] ;  s.wmodeMom1x = [0,0]
# s.wmodeQ0x   = [s.Qx,s.Qx] ; s.wmode__DQx = [-1e-4 + 5e-6j,-1.1e-4+5e-6j]  ; s.wmodeMom0x = [1,1] ;  s.wmodeMom1x = [0,0]
# s.wmodeQ0x   = [s.Qx]  ; s.wmode__DQx = [-1.47e-4 + 1.25e-5j]   ; s.wmodeMom0x = [1] ;  s.wmodeMom1x = [1]  # PRAB case 1
# s.wmodeQ0x   = [s.Qx]  ; s.wmode__DQx = [-8.426306527481442e-05+4.266452711808752e-06j]   ; s.wmodeMom0x = [0.024677627344232712] ;  s.wmodeMom1x = [0]  #LHC2018, 80s40r, Q'=15,2/g=100: -8.426306527481442e-05+4.266452711808752e-06j, eta0=0.024677627344232712, athresh=2.441e-5
s.wmodeQ0x   = [s.Qx]  ; s.wmode__DQx = [-8.400309556233498e-05+4.366429175929166e-06j]   ; s.wmodeMom0x = [0.023930223084810433] ;  s.wmodeMom1x = [0]  #LHC2018, 120s60r, Q'=15,2/g=100: -8.400309556233498e-05+4.366429175929166e-06j, eta0=0.023930223084810433, athresh=2.452e-5
# s.wmodeQ0y   = [s.Qy]  ; s.wmode__DQy = [-120e-6+8e-6j]      ; s.wmodeMom0y = [1] ;  s.wmodeMom1y = [1]


s.flag_wmodesFromFile = False
s.wmodeFilename = ''
if s.flag_wmodesFromFile:
    if s.Qpx==15 and s.gx in [0,0.02] and 1:
        dGain = s.gx/2 + 0.0
        s.wmodeFilename = paths.ToWmodeFiles +'LHCDipoleMoment_idealADT_nSlice120_nRing60'+'_dGain'+str(dGain)+'.pkl'
        with open(s.wmodeFilename,'rb') as pklfile:
            [onegChromas,onegCoherentTuneShifts,onegSideBands,onegDipoleMoments] = pkl.load(pklfile)
    else:
        s.wmodeFilename = paths.ToWmodeFiles + 'LHCDipoleMoment_idealADT_B1_nSlice80_nRing40.pkl'
        with open(s.wmodeFilename,'rb') as pklfile:
            [dGains,chromas,coherentTuneShifts,sideBands,dipoleMoments] = pkl.load(pklfile)
        iGainx   = np.argmin(np.abs(s.gx/2 - dGains))
        onegSideBands = sideBands[iGainx]
        onegCoherentTuneShifts = coherentTuneShifts[iGainx]
        onegDipoleMoments = dipoleMoments[iGainx]
        onegChromas = chromas
    print('setup_PyRADISE.py: Loaded modes from \n  %s'%s.wmodeFilename)

    iChromax = np.argmin(np.abs(s.Qpx-onegChromas))
    s.Qs = 0.00191
    s.wmode__DQx = onegCoherentTuneShifts[iChromax]
    s.wmodeQ0x   = np.ones(np.size(s.wmode__DQx)) * s.Qx # + sideBands[iGainx,iChromax] * s.Qs
    s.wmodeMom0x = onegDipoleMoments[iChromax]
    s.wmodeMom1x = np.zeros_like(s.wmodeMom0x)  # TODO: get actual headtail moments from file!

## iCoeff 3: Noise Excited Wakefield parameters - USHO approach
s.flag_FindAlpha = [1,1]        # update [real,imaginary] part of taylorAlpha (iCoeff=3)
s.flag_UpdateAlpha = 1          # Update taylorAlpha at each timestep or not (iCoeff=3)
s.flag_UpdateReQ = 1            # 1 (0): Update Real(DQ_mLD) every step or not (iCoeff=3)


######################
## Noise parameters ##

## IBS noise
s.sigma_ibsx = np.sqrt(0.02*2/s.f_rev/3600)  * 0
s.sigma_ibsy = s.sigma_ibsx*0

##  White noise
s.sigma_k0x = 1e-4 * 1
s.sigma_k0y = s.sigma_k0x * 1

## CC amplitude noise
s.sigma_k1x = 1e-4 * 0
s.sigma_k1y = s.sigma_k1x * 0

##################################################
## Parameters for Stability diagram Calculation ##
s.nQ = 500                      # Number of tunes to be calculated with the Dispersion integral
s.nStep = 2000                  # Grid resolution for PySSD
s.interpOrderSD = 1             # Interpolation order of the stability diagram
s.widthRatioSD = 0.05           # s.nQ//2 of the free tunes are in a part widthRatioSD of the total range around the most unstable mode
s.flag_center_tune_is_LDQ = 0   # Calculate the center_tune by estimating the Landau damped tune of the least stable mode - not necessary and is time consuming
def set_integrator_epsilon(ax,bx):
    ## integrator_epsilon: Used by PySSD to calculate the dispersion integral
    ## divided by 2 compared to previous standard.
    return 1e-2 * max(abs(ax),abs(bx))  *[1,4][1*(np.abs((bx**2+ax**2)/(bx*ax+1e-16))>10)]
s.integrator_epsilon_scale = .5
s.integrator_epsilon = set_integrator_epsilon(s.ax,s.bx) * s.integrator_epsilon_scale


s.nSD_scaled = [18,0][s.iCoeff>2]               # How many scaled detuning strengths to calc SD for, relevant for iCoeff=2 (without wakefield modes)
s.scales = np.linspace(1.2,.001,s.nSD_scaled,)  # Which scales

s.ntSDmax = s.ntPSImax                          # Max number of PSI to calculate SD for after solving

######################################
######## Plotting parameters #########
s.plot_rmax    = 6        #
s.flag_Fill    = 0        #

###################################
## Setting the name of the study ##
s.name = 'test_calc'
# s.name = 'test_EPJ_Qp15tau100_ax1.5at'


#####################
## Dump setup file ##
def dump_setup(s=s):
    with open(paths.ToPickles + s.name + '_Setup.pkl','wb') as pklfile:
        print('dump_setup: Dumped setup file to \n  %s\n'%pklfile.name)
        pkl.dump(s,pklfile)
    return

if __name__=="__main__":
    ## Save Namespace s to file so that you can load it
    dump_setup(s=s)
