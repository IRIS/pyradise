
# PyRADISE

PyRADISE is a Python code for studying RAdial DIffusion and Stability Evolution.
It numerically solves a PDE (diffusion equation) using a FVM (Finite Volume Method) scheme.
The evolving stability is subsequently evaluated by using PySSD to numerically solve the dispersion integral based on the bunch distribution function from the PDE solver and amplitude detuning.

Many basic Python libraries are required (numpy, scipy, copy, matplotlib, sys, os, time, multiprocessing).

Author : S. V. Furuseth (sondre.vik.furuseth@cern.ch)

## How to use PyRADISE
A clear understanding of what PyRADISE is intended to simulate is important. Try looking up some papers:
- https://doi.org/10.1103/PhysRevAccelBeams.23.114401
- https://doi.org/10.1103/PhysRevAccelBeams.23.034401

To setup a simulation, the parameters can be set in setup_PyRADISE.py.
That includes changing the Namespace called 'paths', especially paths.ToPackage, which is the path to where you have stored both the folder PyRADISE and PySSD.
To run the simulation, you can simply execute the script calc_PyRADISE.py
```
$ python3 calc_PyRADISE.py
```
which will automatically take the input parameters from setup_PyRADISE.py. A more thorough explanation follows below.

<!-- ## Staged calculation (as in v.2.0.0) -->
The calculation in PyRADISE is staged. After each stage, the SolverClass object can be stored.
Whether or not to store after the various stages can be decided by toggling the parameters "flag_SavePkl_S*" in calc_PyRADISE.py
Assume in the following that we call the study 'studyname'.

### Setup: Input parameters
Set the parameters for the study, e.g. in setup_PyRADISE.py.

The key parameters that increase both the memory consumption and wall time of the calculation (and should be set small if you are testing) are:
- Ncx: Number of cells in the horizontal action dimension.
- Ncy: Number of cells in the vertical action dimension.
- ntODE: Number of time steps for which to solve the PDE.
- nQ: Number of tunes in each stability diagram.

There are two ways to make the setup:
```
$ python3 setup_PyRADISE.py
```
will create a file 'studyname_Setup.pkl' that will be stored in the directory paths.ToStorage. You can then load this setup file to initialize PyRADISE.
```
$ python3 calc_PyRADISE.py
```
will load the setup Namespace from setup_PyRADISE.py. If flag_SavePkl_Setup in calc_PyRADISE.py is True, this will store a pickle file 'studyname_Setup.pkl'.

### Stage 0: Initialization
Initialize the SolverClass object based on the setup with the function calc_init() in calc_PyRADISE.py.
If you want to load the setup from a file called 'studyname_Setup.pkl', call
```
$ python3 calc_PyRADISE.py studyname_Setup
```

If flag_SavePkl_S0 in calc_PyRADISE.py is True, the PyRADISE object will be stored in a pickle file named 'studyname_S0.pkl'.
This file will then include the initial distribution, diffusion coefficient, and relevant initial stability diagrams. This can be useful for debugging and for checking that you are about to simulate what you want to.

To check the initialization, use the function testPlots_S0() in plot_PyRADISE.py by calling
```
$ python3 plot_PyRADISE.py studyname_S0
```

### Stage 1: Solve the distribution evolution
Solve the PDE with the function calc_PSI in calc_PyRADISE.py.
If iCoeff is 5, this stage will also achieve the calculation of the stability diagram.
If iCoeff is 3, 4 or 5, the evolution of the stability margin will be calculated during this stage.
At the end of this stage, some distributions may be deleted. This is done to store memory. The maximum number of time steps is set by the parameter 'ntODE'. The maximum number of distributions is set by the parameter 'ntPSImax'.

If flag_SavePkl_S1 in calc_PyRADISE.py is True, the PyRADISE object will be stored in a pickle file named 'studyname_S1.pkl'.
This file will include the evolution of the distribution (and potentially the stability diagram).

To check the distribution evolution, use the function testPlots_S1() in plot_PyRADISE.py by calling
```
$ python3 plot_PyRADISE.py studyname_S1
```

### Stage 2: Calculate the stability diagrams
Calculate the stability diagram and various related quantities all the discrete times for which the distribution was stored in the previous stage, by calling the function calc_SD in calc_PyRADISE.py.
If the stability diagram was calculated during Stage 1 (if iCoeff is 5), it will not be recalculated to save wall time.

If flag_SavePkl_S2 in calc_PyRADISE.py is True, the PyRADISE object will be stored in a pickle file named 'studyname_S2.pkl'.

To check the stability evolution, use the function testPlots_S2() in plot_PyRADISE.py by calling
```
$ python3 plot_PyRADISE.py studyname_S2
```

### Stage 3: Only save key results
If flag_SavePkl_S3 in calc_PyRADISE.py is True, the key results will be stored in a pickle file named 'studyname_S3.pkl'.
For now, that includes quantities related to the latency, but it can be changed by modifying the list in the function dump_results() in calc_PyRADISE.py.
This file requires much less memory than 'studyname_S2.pkl'.

To check the key results, use the function testPlots_S3() in plot_PyRADISE.py by calling
```
$ python3 plot_PyRADISE.py studyname_S3
```

## PySSD in PyRADISE

Copy of PySSD which is available on https://github.com/PyCOMPLETE/PySSD.  
Pulled on February 19 2020.

## Version history

### v.2.0.0
Not backwards compatible. Due to:
- Change of package structure. Can now import PyRADISE and then e.g. use PyRADISE.L2D2_functions.latency_ana to get the analytical latency.
- Change of naming convention
- Inclusion of more physics. One can still simulate what was implemented in v.1.0.0, but it requires a different setup.

New physics:
- Ability to model CC amplitude noise (headtail noise) in addition to dipolar noise.
- iCoeff=5: Diffusion coefficient due to wakefields calculated with the dispersion integral.

New scripts added to aid in using PyRADISE:
- setup_PyRADISE.py: Used to set input parameters
- calc_PyRADISE.py: Used to perform the PyRADISE calculation (based on the setup parameters). Can stage the calculation.
- plot_PyRADISE.py: Makes key plots based on the output of PyRADISE
- _plotconfig.py: Configures how the plots should look like. Loaded by plot_PyRADISE.py.
- Testcases/testcase1.py: A single test case, which should work out of the box (possibly after adjusting the paths in setup_PyRADISE.py).

### v.1.0.0
The first version included all the necessary scripts, but no help on how to use it.
To be included in the future:
- Initialization script(s)
- Example case
