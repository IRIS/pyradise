import os,sys
import numpy as np
import pickle as pkl
import copy
from argparse import Namespace

# sys.path.append(<path to setup_PyRADISE>) # If needed
from setup_PyRADISE import paths, s, dump_setup, set_integrator_epsilon
sys.path.append(paths.ToPackage)
import PyRADISE


## Parameters for this file
## What stages of the calculation should be stored ?
flag_SavePkl_Setup=True         # Only input parameters
flag_SavePkl_S0 = True          # Input parameters, initial Psi, SD and modes, Diffusion coefficient - Good for debugging purposes
flag_SavePkl_S1 = True          # Psi(t) (and SD(t) if calculated while solving)
flag_SavePkl_S2 = True          # Psi(t) and SD(t)
flag_SavePkl_S3 = True          # Only key results: latency, stabilityMargin(t), etc.


#############################
## Check filename for info ##
def checkFilename(filename):

    if len(filename)==0:
        return '',False,-1

    ## Adapt filename
    if filename.find('.pkl')>0:
        filename=filename[:filename.find('.pkl')]
        print(filename)
    elif filename.find('_S')<0:
        filename+='_Setup'
    filename+='.pkl'

    ## Check if file exists
    pathToFile = paths.ToPickles+filename
    flag_PklExists = True
    if not os.path.isfile(pathToFile):
        print('WARNING in checkFilename: %s does not exist'%(pathToFile))
        flag_PklExists = False

    ## Find stage of inputfile
    stage=-1
    if flag_PklExists:
        if filename.find('_Setup')>0:
            stage=-1
        elif filename.find('_S0')>0: stage=0
        elif filename.find('_S1')>0: stage=1
        elif filename.find('_S2')>0: stage=2
        elif filename.find('_S3')>0: stage=3
        else:
            print('ERROR in checkFilename: Do not know what stage this is:\n  %s\nExiting'%pathToFile)
            sys.exit()

    return pathToFile, flag_PklExists, stage

#############################
## Load setup or calc file ##
def load_setup(s=s,pathToFile=''):
    ## Load setup from file
    if len(pathToFile)>0:
        if os.path.isfile(pathToFile):
            with open(pathToFile,'rb') as pklfile:
                s = pkl.load(pklfile)
                print('load_setup: Loaded setup from \n  %s\n'%pathToFile)
        else:
            print('load_setup: File does not exist:\n  %s\n  Exiting.\n'%pathToFile)
            sys.exit()
    else:
        None
        print('load_setup: Loaded setup from setup_PyRADISE.py\n')
    return s

def load_calc(pathToFile):
    print('calc_PyRADISE.py: Loaded an instance of PyRADISE from file:\n  %s'%pathToFile)
    with open(pathToFile,'rb') as pklfile:
        calc = pkl.load(pklfile)
    return calc


##########################################
## Store parameters in relevant objects ##
def calc_init(s=s,flag_SavePkl=flag_SavePkl_S0):
    ############################
    ## Initialize SolverClass ##
    detuning = PyRADISE.Detuning.LinearDetuningClass(s.Qx,s.Qy,s.ax,s.bx,s.ay,s.by)
    noise = PyRADISE.Noise.NoiseClass(  s.f_rev,s.sigma_k0x,s.sigma_k0y,
                                                s.sigma_k1x,s.sigma_k1y,
                                                s.sigma_ibsx,s.sigma_ibsy)
    mach = PyRADISE.Machine.MachineClass(detuning,noise,s.gx,s.gy,s.f_rev,
                        s.wmodeQ0x,s.wmode__DQx,s.wmodeMom0x,s.wmodeMom1x,
                        s.wmodeQ0y,s.wmode__DQy,s.wmodeMom0y,s.wmodeMom1y,
                        Qpx=s.Qpx,Qpy=s.Qpy,Qs=s.Qs,sigma_dpp=s.sigma_dpp,
                        dQxAvg=s.dQxAvg,dQyAvg=s.dQyAvg,damperAlpha=s.damperAlpha)

    grid = PyRADISE.Grid.Grid2DClass(Ncx=s.Ncx,JxMax=s.JxMax,BC_xMax=s.BC_xMax,BC_xVal=s.BC_xVal,
                                     flag_Radial=s.flag_Radial,ND=s.ND,
                                     Ncy=s.Ncy,JyMax=s.JyMax,BC_yMax=s.BC_xMax,BC_yVal=s.BC_xVal)

    calc = PyRADISE.Solver.SolverClass(grid,mach,s.iCoeff,s.tsODE,s.ntPSImax,s.flag_ICPoint,
                      solve_method=s.solve_method,interpOrderPsi=s.interpOrderPsi,
                      flag_FindAlpha=s.flag_FindAlpha, flag_UpdateReQ=s.flag_UpdateReQ ,flag_UpdateAlpha=s.flag_UpdateAlpha,
                      flag_UpdateCoeff=s.flag_UpdateCoeff,flag_UpdateCoeffAllTheWay=s.flag_UpdateCoeffAllTheWay,flag_AdjustTmax=s.flag_AdjustTmax,
                      flag_SDRemoveLoops=s.flag_SDRemoveLoops,relInstabilityThreshold=s.relInstabilityThreshold,debug=s.debug,maxCPU=s.maxCPU)
    calc.parameters = s

    ###############################
    ## Initialize Postprocessing ##
    calc.postProc_init(flag_ProjX=s.flag_ProjX,
                            xMax_ProjX=s.xMax_ProjX)

    ##############################
    ## Initialize SD Calculator ##
    calc.SD_init(ntSDmax=s.ntSDmax,scales=s.scales,
                nQ=s.nQ,integrator_epsilon=s.integrator_epsilon,JMaxSD=s.JMaxSD,nStep=s.nStep,
                interpOrderSD=s.interpOrderSD,widthRatioSD=s.widthRatioSD,flag_center_tune_is_LDQ=s.flag_center_tune_is_LDQ)

    ########################
    ## Initialize plotter ##
    calc.plot_init(  time_scale=s.time_scale,
                        plot_rmax=s.plot_rmax,flag_Fill=s.flag_Fill)

    ###########################
    ## Initialize PDE Solver ##
    if flag_SavePkl:
        ## Note: Needed to calculate diffusion coefficient before storing ..._S0.pkl
        calc.solver_init(flag_calcSD=True)
        calc.FV_M = None  # Not needed for initialization
        calc.BC_V = None  # Not needed for initialization
        None

    ################
    ## Store calc ##
    calcName = s.name
    if flag_SavePkl:
        with open(paths.ToPickles+calcName+'_S0.pkl','wb') as pklfile:
            pkl.dump(calc,pklfile)
    print('calc_init: Initialized PyRADISE for \n  %s\n'%(paths.ToPickles+calcName))
    return calc

###################################
## Step 0 -> 1 (solving the PDE) ##
def calc_PSI(calc,calcName,flag_SavePkl=flag_SavePkl_S1):

    ## Solve
    calc.solve()#(i_dpsidt=1,flag_FindAlpha=1,flag_UpdateReQ=1)

    ## Postprocess
    calc.postProc_distributions()
    calc.postProc_moments(n_moments = 3)

    ## Store
    if flag_SavePkl:
        with open(paths.ToPickles+calcName+'_S1.pkl','wb') as pklfile:
            pkl.dump(calc,pklfile)
    print('calc_PSI: Solved PDE for \n  %s\n'%(paths.ToPickles+calcName))
    return calc

##################################################
## Step 1 -> 2 (Calculating stability diagrams) ##
def calc_SD(calc,calcName,scale=None,flag_SavePkl=flag_SavePkl_S2):
    planes = [0,1]
    if calc.iCoeff >2 and 1:
        planes = []
        planes = []
        if calc.M.nWmodex>0: planes += [0]
        if calc.M.nWmodey>0: planes += [1]

    ## Scale det detuning (and integrator_epsilon) if wanted.
    if not (scale==None):
        calc.integrator_epsilon*=scale
        calc.M.Q.scale(scale)
        calc.M.Qy.scale(scale)

    ## Calculate SD for each distribution
    calc.SD_calcEvolution(planes=planes)

    ## Calculate SD for scaled detuning of Psi0
    if len(calc.scales)>0:
        calc.SD_calcScaledPsi0(plane=0)
        ## If equal detuning in both planes, the SD will be identical and can be copied instead of recalculated
        calc.SD_copyScaledPsi0(planeFrom=0)

    ## StabilityDiagram Postprocessing
    calc.SD_calcStabilityMargin(plane=0,)
    calc.SD_calcStabilityMargin(plane=1,)

    calc.SD_calcEffectiveStrength(plane=0,maxmin_ratio=100)
    calc.SD_calcEffectiveStrength(plane=1,maxmin_ratio=100)

    ## Calculate growth rate of the mode that is least stable in the end
    if calc.M.nWmodex>0:
        calc.SD_calcGrowthRate(plane=0, DQ_mCoh = calc.M.wmode__DQx[calc.indLeastStablex[-1]])
    if calc.M.nWmodey>0:
        calc.SD_calcGrowthRate(plane=1, DQ_mCoh = calc.M.wmode__DQx[calc.indLeastStablex[-1]])

    ## Store
    if flag_SavePkl:
        addname = '' if scale==None else "_scale%.2f"%scale
        with open(paths.ToPickles+calcName+'_S2%s.pkl'%addname,'wb') as pklfile:
            pkl.dump(calc,pklfile)
    print('calc_SD: Calculated SD for \n  %s\n'%(paths.ToPickles+calcName))
    return calc

##########################################
## Step 2 -> 3 (only store key results) ##
def dump_results(calc,s,flag_SavePkl=flag_SavePkl_S3):
    ## Save only the most necessary
    calc2 = Namespace()
    wantedAttributes = ['tsODE','parameters','indexSD','indexPSI',
                        'latQuadx','latNumx',
                        'latQuady','latNumy',
                        'indexSMx','stabilityMarginx','indLeastStablex','effectiveStrengthx','growthRatex',
                        'indexSMy','stabilityMarginy','indLeastStabley','effectiveStrengthy','growthRatey']
    for attname in wantedAttributes:
        if attname in dir(calc):
            setattr(calc2,attname,getattr(calc,attname))
        else:
            print('WARNING in dump_results: Could not find attribute %s'%attname)
            setattr(calc2,attname,None)

    if flag_SavePkl:
        with open(paths.ToPickles+s.name+'_S3.pkl','wb') as pklfile:
            pkl.dump(calc2,pklfile)
            print('dump_results: Stored key results for \n  %s'%pklfile.name)
    return calc2

###########################
###########################


if __name__ =="__main__":
    ## Check if there is a file to be loaded
    filename = ''
    if len(sys.argv)>1:
        filename = sys.argv[1]
    pathToFile,flag_PklExists,stage = checkFilename(filename)

    ## Get input
    if flag_PklExists and stage>=0:
        ## Load PyRADISE instance from file
        calc = load_calc(pathToFile)
        s = calc.parameters
    elif stage<0:
        ## Get setup, either from file or setup_PyRADISE.py
        s = load_setup(pathToFile=['',pathToFile][flag_PklExists])


        ## Change setup
        # s.iCoeff = 5
        # s.integrator_epsilon = s.integrator_epsilon * .1

        if flag_SavePkl_Setup:
            dump_setup(s)

    # if np.size(s.wmode__DQx)>0  and 0:
    #     iMode = np.argmax(np.imag(s.wmode__DQx))
    #     ax_thresh,_,_,_=PyRADISE.L2D2_functions.find_axThreshold(DQ_mCoh=s.wmode__DQx[iMode],Qx=s.Qx,asign=np.sign(s.ax),bafac=s.bx/s.ax,
    #                                                             tol_search=1e-5,method=2)
    #
    #     emittNormx = 2e-6
    #     energy_MeV = 6.5e6
    #     Ioct_thresh = PyRADISE.L2D2_functions.axToIoct(ax_thresh,beamNumber=1,emittNormx=emittNormx,energy_MeV=energy_MeV)
    #     print('setup_PyRADISE.py: Stability threshold in the horizontal plane is with a_thresh=%.4e'%ax_thresh,
    #         '\n                   That corresponds to Ioct=%.0f A for normalized emittance=%.2e um and E=%.2e TeV'%(
    #                                                                 Ioct_thresh,emittNormx*1e6,energy_MeV*1e-6),
    #         '\n                   Hence, stability margin is a_x/a_thresh=%.3f'%(s.ax/ax_thresh),
    #         '\n                   This is for a mode ΔQ_coh=%.3e+i%.3e'%(np.real(s.wmode__DQx[iMode]),np.imag(s.wmode__DQx[iMode])))
    #
    #     print()

    ## PyRADISE calculation
    if stage<0:
        calc = calc_init(s=s)
    if stage<1:
        calc = calc_PSI(calc,s.name)
    if stage<2:
        calc = calc_SD(calc,s.name)
    if stage<3:
        dump_results(calc,s=s)
