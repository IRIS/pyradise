import numpy as np
import sys , os
import pickle as pkl
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes,mark_inset
plt.style.use('classic')
mpl.interactive(True)

from _plotconfig import plotconfig
plotconfig()

from setup_PyRADISE import paths
from calc_PyRADISE import checkFilename
sys.path.append(paths.ToPackage)
mpl.rcParams["savefig.directory"] = paths.ToPlots   # Set standard path for where to save plots
import PyRADISE


#############################################
## Test initialization by making key plots ##
def testPlots_S0(calc):
    print('Test plots of S0')

    ## Distribution interpolation
    if calc.ND==1:
        calc.calcInterpDist(calc.psi0,0,flag_BC_gradient_0=1)
        fig1,ax1 = plt.subplots(1)
        fig2,ax2 = plt.subplots(1)
        Jxs = np.linspace(0,1,10001) * (calc.G.JxMax+2) - 1
        psi1D , dPsi1DdJx = calc.getPsi1D(calc.psi0)
        i=0
        ax1.plot(calc.G.Jxc,psi1D[0],'x',c='C%d'%i)#label=r'$J_y=%.2f$'%Jy,c='C%d'%i)
        ax1.plot(Jxs,calc.interpDistx.getValue(Jxs,0),'-',c='C%d'%i)
        ax2.plot(calc.G.Jxbx[1:-1],dPsi1DdJx[0,:],'x',c='C%d'%i)#label=r'$J_y=%.2f$'%Jy,c='C%d'%i)
        ax2.plot(Jxs,calc.interpDistx.getDJx(Jxs,0),'-',c='C%d'%i)

    if calc.ND==2:
        ## 2D plot
        out=calc.plot_Dist2D(flag_Change=0,iTime=0,iStyle=1,interp=0,nlevels=30)#,xMax=JMax,yMax=JMax)
        out[1].set_title('2D Distribution')
        out=calc.plot_Dist2D(flag_Change=0,iTime=0,iStyle=1,interp=1,nlevels=30)#,xMax=JMax,yMax=JMax)
        out[1].set_title('Interpolated distribution')

        ## 1D plot
        calc.calcInterpDist(calc.psi0,0,flag_BC_gradient_0=1)
        calc.calcInterpDist(calc.psi0,1,flag_BC_gradient_0=False)

        ## 1D curves for single values of Jy
        fig1,ax1 = plt.subplots(1)
        fig2,ax2 = plt.subplots(1)
        Jys = [0,1,2]
        Jxs = np.linspace(0,1,10001) * (calc.G.JxMax+2) - 1
        # psi2D = calc.psi0.reshape((calc.G.nrow,calc.G.ncol))
        psi2D,dPsi2DdJx,dPsi2DdJy = calc.getPsi2D(calc.psi0)
        for i,Jy in enumerate(Jys):
            if Jy>=0:
                iJy = np.where(calc.G.Jyc>Jy)[0][0]
                Jy = calc.G.Jyc[iJy]
            ax1.plot(calc.G.Jxc,psi2D[iJy,:],'x',label=r'$J_y=%.2f$'%Jy,c='C%d'%i)
            ax1.plot(Jxs,calc.interpDistx.getValue(Jxs,Jy),'-',c='C%d'%i)
            ax2.plot(calc.G.Jxbx[1:-1],dPsi2DdJx[iJy,:],'x',label=r'$J_y=%.2f$'%Jy,c='C%d'%i)
            ax2.plot(Jxs,calc.interpDistx.getDJx(Jxs,Jy),'-',c='C%d'%i)
        ax1.legend(loc=0)
        ax2.legend(loc=0)
    ax1.set_ylabel(r'$\Psi$')
    ax2.set_ylabel(r'$\mathrm{d}\Psi/\mathrm{d}J_x$')
    for ax in [ax1,ax2]:
        ax.set_title('Test interpolation of the distribution')
        ax.set_xlabel(r'$J_x$')
        ax.grid(True)
        plt.gcf().tight_layout()

    ## Diffusion coefficient as function of incoherent tune
    DQ_inco = PyRADISE.PySSDHelper.get_tune_range(detuning=calc.M.Q,maxJ=calc.G.JMax,n_samples=1000,margin=calc.M.Q.ax*2) - calc.M.Q.Q0x
    fig,axs = plt.subplots(2,1)
    for plane in range(2):
        Dunitlabel='[1/s]'
        Dunit = 1
        if [calc.M.N.D_k0x,calc.M.N.D_k0y][plane]>0:
            Dunitlabel='$[D_0]$'
            Dunit = [calc.M.N.D_k0x,calc.M.N.D_k0y][plane]
        elif [calc.M.N.D_k1x,calc.M.N.D_k1y][plane]>0:
            Dunitlabel='$[D_1]$'
            Dunit = [calc.M.N.D_k1x,calc.M.N.D_k1y][plane]

        axs[plane].plot(DQ_inco,PyRADISE.Coefficients.DiffCoeffDQ(calc.M,DQ_inco,plane,calc.iCoeff)[0]/Dunit)
        axs[plane].set_ylim(bottom=0)
        planeChar = ['x','y'][plane]
        axs[plane].set_xlabel(r'$Q_%s-Q_{%s0}$'%(planeChar,planeChar))
        axs[plane].set_ylabel(r'$D_%s$ %s'%(planeChar,Dunitlabel))
    plt.tight_layout()

    ## Diffusion coefficient as function of grid
    if calc.ND==1 :
        DDx,_,err=PyRADISE.Coefficients.DiffCoeffGrid(calc.M, calc.G,calc.iCoeff)
        plt.figure()
        plt.plot(calc.G.Jxbx,DDx[0]/calc.G.X[0])#/calc.G.Jxc1D)
        plt.title('Diffusion coefficient at grid points')
    else:
        out=calc.plot_Coeff2D(iCoeffType=0,plane=0,nDec=3,xMax=10)#,vlmax=1e-6) #vlmax=1e-6
        out=calc.plot_Coeff2D(iCoeffType=0,plane=1,nDec=3)

    ## Stability diagram and modes
    calc.SD_plotSD(plane=0,title='Horizontal plane')
    calc.SD_plotSD(plane=1,title='Vertical plane')

    ## Finish
    return

#########################################
## Test PDE solver by making key plots ##
def testPlots_S1(calc):
    print('Test plots of S1')
    #############
    ## Moments ##
    fig1,ax1 = calc.plot_moments(plane=0)
    fig2,ax2 = calc.plot_intensity(fmt='-o')


    ##################
    ## Distribution ##
    if calc.ND==1 :
        # calc.plot_psis1Dx(iCoord=0,flag_Fill=1)
        # calc.plot_psis1Dx(iCoord=1,plot_step=1)
        # calc.plot_psis1Dx(iCoord=2)      # projected on x-axis
        calc.plot_dPsis1Dx()

    elif calc.ND==2:
        JMax = min(calc.G.JxMax,calc.G.JyMax)
        iTime=-1  # Index of which distribution should be plotted

        out      =calc.plot_Dist2D(flag_Change=0,iTime=iTime,iStyle=1,interp=1,nlevels=30,xMax=JMax,yMax=JMax)
        fig,ax,cb=calc.plot_Dist2D(flag_Change=1,iTime=iTime,iStyle=1,interp=1,nlevels=30,xMax=JMax,yMax=JMax,nDec=4)
        fig.tight_layout()#pad=1.08)

    ######################
    ## Stability margin ##
    out=calc.SD_plotStabilityMargin(plane=0,flag_Relative=1,flag_Normalize=0,label='',flag_PlotInscribed=0,ylabel=r'Stability margin')
    out=calc.SD_plotStabilityMargin(plane=1,flag_Relative=1,flag_Normalize=1,label='y',flag_PlotInscribed=0,)#ax=out[1])

    if calc.flag_storeSDWhileSolving and calc.ntSD>0 and 1:
        testPlots_S2(calc,flag_StabilityMargin=0,flag_EffectiveStrength=0)

    ## Finish
    return

#############################################
## Test SD calculation by making key plots ##
def testPlots_S2(calc,flag_GrowthRate=1,flag_EffectiveStrength=1,flag_StabilityMargin=1):
    ## Stability diagrams with all modes
    flag_FillSD = False
    out=calc.SD_plotAllSD(plane=0,flag_FillSD=flag_FillSD,flag_PlotModes=1,iTime_step=1,flag_SDConstituents=0,marker='.')
    if np.size(out)>1: out[1].set_title('Horizontal')
    out=calc.SD_plotAllSD(plane=1,flag_FillSD=flag_FillSD,flag_PlotModes=1,iTime_step=1,flag_SDConstituents=0,marker='.')
    if np.size(out)>1: out[1].set_title('Vertical')

    calc.SD_plotWaterfall(plane=0)
    calc.SD_plotWaterfall(plane=1)

    ## Growth rate
    if flag_GrowthRate:
        # calc.SD_calcGrowthRate(plane=0, DQ_mCoh = calc.M.wmode__DQx[0])
        out=calc.SD_plotGrowthRate(plane=0,flag_Normalize=0,marker='x',label='Horizontal')
        # if np.size(out)>1: out[1].set_xscale('log')
        out=calc.SD_plotGrowthRate(plane=1,flag_Normalize=0,marker='+',label='Vertical')

    ## Stability margin
    if flag_StabilityMargin:
        calc.SD_calcStabilityMargin(plane=0)
        calc.SD_calcStabilityMargin(plane=1)
        out=calc.SD_plotStabilityMargin(plane=0,flag_Relative=1,flag_Normalize=1,label='x',flag_PlotInscribed=0)
        out=calc.SD_plotStabilityMargin(plane=1,flag_Relative=1,flag_Normalize=1,label='y',flag_PlotInscribed=0,ax=out[1])

    ## Effective strength
    if flag_EffectiveStrength and np.size(calc.scales)>0:
        ratio=100
        calc.SD_calcEffectiveStrength(plane=0,flag_InterpStrength=1,wmode_DQ_R=0,flag_AllReDQ=1,maxmin_ratio=ratio,interpOrderSD=1)
        calc.SD_plotEffectiveStrength(plane=0,flag_PlotInscribed=True ,flag_FillSD=False)
    return


def zoomedSD(calc,plane=0,flag_NormQ=False,iTime_step=1,flag_FillSD=False,flag_HideInscribedTicks=0):
    """
    Plot the evolution of the stability diagram, including a zoom on a particular area.
    Useful for seeing the drilling of a hole in the stability diagram in one area, while nothing else changes.
    """
    absax = abs(calc.M.Q.ax)
    Qunit = 1
    if flag_NormQ:
        Qunit = absax

    ## Basic SD plot
    tmax = calc.tmax
    out = calc.SD_plotAllSD(plane=plane,flag_PlotModes=1,flag_FillSD=flag_FillSD,tmax=tmax,flag_NormQ=flag_NormQ,iTime_step=iTime_step)
    if np.size(out)<=1:
        return
    fig1,ax1 = out
    ax1.grid(False)
    ax1.set_xlim(left=-6*absax/Qunit,right=16*absax/Qunit)

    ## Inscribed axis
    x1, x2, y1, y2 = np.array([-3, -1, 0.1, 0.6])*absax
    width="47%"
    height="60%"
    print(  'zoomedSD: The axis limits of the inscribed axis is set in zoomedSD.',
          '\n          To zoom somewhere else, change x1,x2,y1,y2.')
    axins = inset_axes(ax1, width=width,height=height,borderpad=0.1) # zoom-factor: 2.5, location: upper-left
    mark_inset(ax1, axins, loc1=2, loc2=4, fc="none", ec="0.5")
    calc.SD_plotAllSD(plane=plane,flag_PlotModes=1,flag_FillSD=flag_FillSD,ax=axins,tmax=tmax,flag_NormQ=flag_NormQ,iTime_step=iTime_step)
    if flag_HideInscribedTicks:
        axins.xaxis.set_ticks(ax1.get_xticks())
        axins.yaxis.set_ticks(ax1.get_yticks())
        axins.set_xticklabels([])
        axins.set_yticklabels([])
    else:
        axins.xaxis.set_major_locator(plt.MaxNLocator(4))
        axins.yaxis.set_major_locator(plt.MaxNLocator(4))
    axins.set_xlim(x1/Qunit+1e-14, x2/Qunit-1e-14) # apply the x-limits
    axins.set_ylim(y1/Qunit+1e-14, y2/Qunit-1e-14) # apply the y-limits
    axins.set_xlabel('')
    axins.set_ylabel('')
    axins.grid(False)

    ## Fix layout
    fig1.tight_layout()
    return

def testPlots_S3(calc):
    ## Print the latency
    print('testPlots_S3: Numerical  Horizontal Latency = %.3e s'%(calc.latNumx))
    print('testPlots_S3: Analytical Horizontal Latency = %.3e s'%(calc.latQuadx))
    print('testPlots_S3: Numerical  Vertical Latency = %.3e s'%(calc.latNumy))
    print('testPlots_S3: Analytical Vertical Latency = %.3e s'%(calc.latQuady))

    ## Stability margin
    plt.figure()
    plt.ylabel(r'$\operatorname{Im}\{ \Delta Q_\mathrm{SD}-\Delta Q_\mathrm{mode}\}$')
    if np.size(calc.stabilityMarginx)>1:
        plt.plot(calc.tsODE[calc.indexSMx],calc.stabilityMarginx,'-1',ms=10,label='Horizontal')
    if np.size(calc.stabilityMarginy)>1:
        plt.plot(calc.tsODE[calc.indexSMy],calc.stabilityMarginy,'-2',ms=10,label='Vertical')
    if np.size(calc.stabilityMarginx)>1 and np.size(calc.stabilityMarginy)>1:
        plt.legend(loc=0)
    plt.xlabel('t [s]')
    plt.tight_layout()

    ## Which mode is least stable?
    plt.figure()
    plt.title('Index of least stable mode')
    if np.size(calc.stabilityMarginx)>1:
        plt.plot(calc.tsODE[calc.indexSMx],calc.indLeastStablex,'-1',ms=10,label='Horizontal')
    if np.size(calc.stabilityMarginy)>1:
        plt.plot(calc.tsODE[calc.indexSMy],calc.indLeastStabley,'-2',ms=10,label='Vertical')
    if np.size(calc.stabilityMarginx)>1 and np.size(calc.stabilityMarginy)>1:
        plt.legend(loc=0)
    plt.xlabel('t [s]')
    plt.tight_layout()

    return

if __name__=="__main__":
    filename = sys.argv[1]
    # inFilename,flag_PklExists = checkFilename(filename)
    pathToFile, flag_PklExists, stage = checkFilename(filename)
    if flag_PklExists:
        with open(pathToFile,'rb') as pklfile:
            calc = pkl.load(pklfile)
        print('Loaded file for testplots: %s'%pathToFile)


        if stage==0 and flag_PklExists:
            testPlots_S0(calc)
        if stage==1 and flag_PklExists:
            testPlots_S1(calc)
        if stage==2 and flag_PklExists:
            testPlots_S2(calc)
            zoomedSD(calc,plane=0,flag_NormQ=False,iTime_step=1,flag_FillSD=False,flag_HideInscribedTicks=1)
            zoomedSD(calc,plane=1,flag_NormQ=False,iTime_step=1,flag_FillSD=False,flag_HideInscribedTicks=0)
        if stage==3 and flag_PklExists:
            testPlots_S3(calc)

        ## To show plots before exiting
        input()
