class NoiseClass(object):
    def __init__(self,f_rev,sigma_k0x,sigma_k0y,
                            sigma_k1x,sigma_k1y,
                            sigma_ibsx,sigma_ibsy):
        ## Rigid dipolar white noise (Damper, Noise excited wakedields,)
        self.sigma_k0x = sigma_k0x
        self.sigma_k0y = sigma_k0y
        self.D_k0x = f_rev*self.sigma_k0x**2/2
        self.D_k0y = f_rev*self.sigma_k0y**2/2

        ## Headtail white noise (Crab cavity amplitude noise)
        self.sigma_k1x = sigma_k1x
        self.sigma_k1y = sigma_k1y
        self.D_k1x = f_rev*self.sigma_k1x**2/2
        self.D_k1y = f_rev*self.sigma_k1y**2/2

        # IBS, incoherent noise
        self.sigma_ibsx = sigma_ibsx
        self.sigma_ibsy = sigma_ibsy
        self.D_ibsx = f_rev*sigma_ibsx**2/2
        self.D_ibsy = f_rev*sigma_ibsy**2/2
