import numpy as np
import scipy as sc
import multiprocessing as mp
import time,copy
import os

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cm

from PySSD.Integrator      import Integrator #FixedTrapezoidalIntegrator, SimpsonIntegrator, TrapzIntegrator, DblquadIntegrator
from PySSD.Distribution    import Gaussian

import PyRADISE


## Calculate stability diagram
def calc_Stability_Diagram_OneQ(integrator1,integrator_epsilon,q):
    eps = integrator_epsilon
    deps= integrator_epsilon/2

    DQ1 = integrator1.integrate(q)
    DQ2 = integrator1.integrate(q+1j*deps)
    DQ3 = integrator1.integrate(q+2j*deps)

    ## Second order Taylor, expanded from DQ2  (divide last term by 2!)
    DQ = DQ2 + (DQ3-DQ1)/(2*deps) * (-eps-deps) + (DQ1-2*DQ2+DQ3)/deps**2 * (-eps-deps)**2/2

    ## First order Taylor, expanded from DQ1
    # DQ = DQ1 + (DQ2-DQ1)/deps * (-eps)

    ## Less accurate First order Taylor (assume taylorAlpha=1)
    # DQ = DQ1 - 1j*integrator_epsilon

    ## Least accurate
    # DQ = DQ1
    return DQ

def calc_Stability_Diagram(distribution,detuning,Q_free,integrator_epsilon,JMaxSD=18,nStep=2000,debug=0,i=0,plane=0):
    ## Set the integrator - Only set 1 to reduce memory usage
    integrator1 = Integrator(distribution, detuning, maxJ=JMaxSD,epsilon=integrator_epsilon,  nStep=nStep)

    ## Start calculation
    t0 = time.time()

    DQ_SD = np.zeros((len(Q_free)), dtype='complex')

    for k, q in enumerate(Q_free):
        DQ_SD[k] = calc_Stability_Diagram_OneQ(integrator1,integrator_epsilon,q)

    # Timing
    t1 = time.time()-t0
    if debug>0 and (i<10 or i%10==0):
        print("calc_Stability_Diagram: Elapsed time calculating a stability diagram (%d,%s): %.2fs."%(i,['x','y'][plane],t1))

    return (i,plane,DQ_SD,Q_free)


#############
### Class ###
class StabilityDiagramClass(object):
    def SD_init(self,ntSDmax=2,scales=[],nQ=100,integrator_epsilon=2e-7,JMaxSD=18,nStep=2000,
                interpOrderSD=1,widthRatioSD=1,flag_center_tune_is_LDQ=0,flag_Print=True):

        self.ntSDmax = ntSDmax
        self.setTimeSD()

        self.nQ     = nQ
        self.integrator_epsilon=integrator_epsilon
        self.JMaxSD = JMaxSD
        self.nStep = nStep

        self.interpOrderSD = interpOrderSD
        self.widthRatioSD = widthRatioSD
        self.flag_center_tune_is_LDQ= flag_center_tune_is_LDQ

        self.scales = scales


        self.flag_SDAllx = False
        self.flag_SDAlly = False


        # Storage of Stability diagrams
        self.DQ_SDAllx = None ; self.Q_freeAllx = None
        self.DQ_SDAlly = None ; self.Q_freeAlly = None
        if self.flag_storeSDWhileSolving:
            self.DQ_SDAllx = np.nan*np.zeros((self.ntODE,self.nQ),dtype='complex')
            self.Q_freeAllx= np.nan*np.zeros((self.ntODE,self.nQ))
            self.DQ_SDAlly = np.nan*np.zeros((self.ntODE,self.nQ),dtype='complex')
            self.Q_freeAlly= np.nan*np.zeros((self.ntODE,self.nQ))


        if flag_Print:
            self.SD_print()


        ## Local variables
        self._DQ_coh_R_label = r"$\operatorname{Re}\{ \Delta Q_\mathrm{coh}\}$"
        self._DQ_coh_I_label = r"$\operatorname{Im}\{ \Delta Q_\mathrm{coh}\}$"
        self._DQ_LD_R_label = r"$\operatorname{Re}\{ \Delta Q_\mathrm{LD}\}$"
        self._DQ_LD_I_label = r"$\operatorname{Im}\{ \Delta Q_\mathrm{LD}\}$"
        self._DQ_SD_R_label = r"$\operatorname{Re}\{ \Delta Q_\mathrm{SD}\}$"
        self._DQ_SD_I_label = r"$\operatorname{Im}\{ \Delta Q_\mathrm{SD}\}$"


    def SD_print(self):
        print('StabilityDiagramClass is initialized with:\n  nQ=%d\n  JMaxSD=%.1f\n  nStep=%d\n  integrator_epsilon=%.1e'%(
                    self.nQ,self.JMaxSD,self.nStep,self.integrator_epsilon),
                    '\n  interpOrderSD=%d'%self.interpOrderSD,
                    '\n  widthRatioSD=%.2f'%self.widthRatioSD,
                    '\n  flag_center_tune_is_LDQ=%d'%self.flag_center_tune_is_LDQ)

    #######################################################
    ## Worker functions used for the following functions ##
    def _getQfree(self,detuning,center_tune,widthRatioSD,plane,distribution):
        margin = max(abs(detuning(1,0)-detuning(0,0)),abs(detuning(0,1)-detuning(0,0)))*1
        if [self.M.nWmodex,self.M.nWmodey][plane]==0 or widthRatioSD==1:
            Q_free = PyRADISE.PySSDHelper.get_tune_range(detuning,margin=margin,n_samples=self.nQ,maxJ=self.G.JMax)
        else:
            ## Horizontal plane
            if plane==0:
                if not np.isfinite(center_tune):
                    ## Choose mode with largest imaginary part
                    ind = np.argmax(self.M.wmode__DQx.imag)
                    if np.any(self.indLeastStablex>=0):
                        ## If have found the least stable mode, use that instead.
                        ind = self.indLeastStablex[self.indLeastStablex>=0][-1]
                    center_tune = self.M.Q.Q0x + self.M.wmode__DQx[ind].real
                    if self.flag_center_tune_is_LDQ:
                        DQ_mLD,DQ_mFree,DQ_mSD,_,_,_ = self.SD_calcGrowthRateOneModeOnePsi(self.M.Q.Q0x,self.M.wmode__DQx[ind],distribution,detuning,
                                                                            taylorAlphaOld=self.M.taylorAlphax[ind],debug=0)
                        center_tune = self.M.Q.Q0x + DQ_mFree

                Qmin = np.min(self.M.Q.Q0x + self.M.wmode__DQx.real)
                Qmax = np.max(self.M.Q.Q0x + self.M.wmode__DQx.real)

            ## Vertical plane
            else:
                if not np.isfinite(center_tune):
                    ## Choose mode with largest imaginary part
                    ind = np.argmax(self.M.wmode__DQy.imag)
                    if np.any(self.indLeastStabley>=0):
                        ## If have found the least stable mode, use that instead.
                        ind = self.indLeastStabley[self.indLeastStabley>=0][-1]
                    center_tune = self.M.Q.Q0y + self.M.wmode__DQy[ind].real
                    if self.flag_center_tune_is_LDQ:
                        DQ_mLD,DQ_mFree,DQ_mSD_,_,_ = self.SD_calcGrowthRateOneModeOnePsi(self.M.Q.Q0y,self.M.wmode__DQy[ind],distribution,detuning,
                                                                            taylorAlphaOld=self.M.taylorAlphay[ind],debug=0)
                        center_tune = self.M.Q.Q0y + DQ_mFree

                Qmin = np.min(self.M.Q.Q0y + self.M.wmode__DQy.real)
                Qmax = np.max(self.M.Q.Q0y + self.M.wmode__DQy.real)

            Q_free = PyRADISE.PySSDHelper.get_tune_range_focused(detuning,margin=margin, n_samples=self.nQ,maxJ=self.G.JMax,
                                         center_tune=center_tune, widthRatioSD=widthRatioSD)
            Q_free[0]  = min(Q_free[0] , Qmin)
            Q_free[-1] = max(Q_free[-1], Qmax)

        return Q_free

    def _workerSD(self,args):
        """
        Function to call calc_Stability_Diagram using multiprocessing
        """
        [i,plane,distribution,detuning,Q_free,integrator_epsilon]  = args
        return calc_Stability_Diagram(distribution,detuning,Q_free,
                                        integrator_epsilon=integrator_epsilon,JMaxSD=self.JMaxSD,nStep=self.nStep,
                                        debug=self.debug,i=i,plane=plane)

    def _interpSD(self,DQ_SD):
        DQ_SD_R = DQ_SD.real
        DQ_SD_I = DQ_SD.imag
        arr1 = np.diff(DQ_SD_R)>0
        arr2 = 1+np.where(np.diff(arr1)>0)[0]
        arr2 = np.concatenate(([0],arr2,[np.size(DQ_SD_R)]))
        nfunc = np.size(arr2)-1

        funcs = ['']*nfunc
        for i in range(nfunc):
            fill_value = ( [DQ_SD_I[0],1][i>0]  ,  [DQ_SD_I[-1],1][i<nfunc-1] )
                # 0 left and right,
                # 1 (almost infinite) where the function is not relevant
            kind = ['nearest','linear','quadratic','cubic'][self.interpOrderSD]
            funcs[i] = sc.interpolate.interp1d(DQ_SD_R[arr2[i]:arr2[i+1]+1],DQ_SD_I[arr2[i]:arr2[i+1]+1],
                                           kind=kind,bounds_error=False,fill_value=fill_value)
        return funcs

    def SD_removeLoops(self,DQ_SD):
        if np.size(DQ_SD)<=1:
            return DQ_SD

        funcs = self._interpSD(DQ_SD)
        nfunc = len(funcs)

        x = DQ_SD.real
        y = DQ_SD.imag
        xNoLoop = x.copy()
        yNoLoop = y.copy()

        for ifunc1 in range(nfunc):
            func1=funcs[ifunc1]

            for ifunc2 in range(ifunc1+1,nfunc):
                func2=funcs[ifunc2]

                ## Get x values to test crossing of functions
                xlim=[max(func1.x[0],func2.x[0])+1e-10,min(func1.x[-1],func2.x[-1])-1e-10]
                if xlim[0]>xlim[1]:
                    continue
                xtest=np.linspace(xlim[0],xlim[1],1000)

                ## Check for crossing
                ytest1=func1(xtest)
                ytest2=func2(xtest)
                indNode = np.nonzero(np.diff(np.sign(ytest1 - ytest2)))[0]

                if np.size(indNode)>0:
                    ## Get crossing point
                    xNode=xtest[indNode[0]]
                    yNode=ytest1[indNode[0]]
                    # print('indnode',indNode,xNode,yNode)

                    ## Get values of x used to interpolate in correct order
                    xfunc1=func1.x
                    if np.nonzero(x==xfunc1[0])[0][0]>np.nonzero(x==xfunc1[1])[0][0]:
                        xfunc1=xfunc1[::-1]
                    xfunc2=func2.x
                    if np.nonzero(x==xfunc2[0])[0][0]>np.nonzero(x==xfunc2[1])[0][0]:
                        xfunc2=xfunc2[::-1]

                    ## Get index of first and last point in the loop
                    indx1 = np.nonzero(np.diff(np.sign(xfunc1-xNode)))[0][0]+1
                    ind1  = np.nonzero(x==xfunc1[indx1])[0][0]
                    indx2 = np.nonzero(np.diff(np.sign(xfunc2-xNode)))[0][0]+1
                    ind2  = np.nonzero(x==xfunc2[indx2])[0][0]
                    xNoLoop[ind1:ind2]=xNode
                    yNoLoop[ind1:ind2]=yNode
        return xNoLoop + 1j*yNoLoop

    ##################################################################
    ## Calculate SD for one distribution calculated with the solver ##
    def SD_calcSinglePsi(self,psi,planes=[0,1],center_tune=np.nan):
        ## Set the width of SD
        widthRatioSD=self.widthRatioSD

        for plane in planes:
            ## Calculate SD
            distribution = self.calcInterpDist(psi,plane,flag_BC_gradient_0=True)
            detuning = [self.M.Q, self.M.Qy][plane]
            Q_free = self._getQfree(detuning,center_tune,widthRatioSD,plane,distribution=distribution)
            args = [0,plane,distribution,detuning,Q_free,self.integrator_epsilon]
            results = self._workerSD(args)

            ## Extract results
            i,plane,res,res2 = results
            if plane==0:
                if np.any(res2!=Q_free):
                    print('ERROR in SD_calcSinglePsi: Not same input Q_free as output...')
                    print('  input:  ',Q_free)
                    print('  output: ',res2)
                self.M.Q_freex = res2
                self.M.DQ_SDx = res
            else:
                if np.any(res2!=Q_free):
                    print('ERROR in SD_calcSinglePsi: Not same input Q_free as output...')
                    print('  input:  ',Q_free)
                    print('  output: ',res2)
                self.M.Q_freey = res2
                self.M.DQ_SDy  = res
        return

    def SD_calcSinglePsiParallel(self,psi,planes=[0,1]):
        """
            NOT IN USE.
            Calculates the stability diagram for a single distribution.
            Parallelized using multiprocessing.apply_async. This can require a lot of memory, so not in use as standard.
        """
        ## Set the width of SD
        center_tune=np.nan
        widthRatioSD=self.widthRatioSD

        ## Paralellisation preperation
        nProcs = np.min([self.maxCPU,self.nQ,(mp.cpu_count()+1)//2])
        nProcsPerPlane = max(1,nProcs//np.size(planes))
        indexes = np.linspace(0,self.nQ,nProcsPerPlane+1,dtype=np.int)
        if nProcs==0:
            print('WARNING in SD_calcSinglePsiParallel: No processes needed? nProcs=0')
            return -1
        print("SD_calcSinglePsiParallel: %d processes (ncpus=%d)"%(nProcs,mp.cpu_count()),nProcsPerPlane)
        pool = mp.Pool(processes = nProcs)

        def collect_result(res):
            results.append(res)
        count=0
        procs = []
        results = []   # must be after def collect_result...

        for plane in planes:
            detuning = [self.M.Q, self.M.Qy][plane]
            distribution = self.calcInterpDist(psi,plane,flag_BC_gradient_0=True)
            Q_free = self._getQfree(detuning,center_tune,widthRatioSD,plane,distribution=distribution)

            if plane==0:
                self.M.Q_freex = Q_free
                self.M.DQ_SDx = np.zeros(self.nQ,dtype=np.complex)
            else:
                self.M.Q_freey = Q_free
                self.M.DQ_SDy = np.zeros(self.nQ,dtype=np.complex)


            for i in range(nProcsPerPlane):
                args = [i,plane,distribution,detuning,Q_free[indexes[i]:indexes[i+1]],self.integrator_epsilon]
                p = pool.apply_async(self._workerSD, args = (args,),callback = collect_result)
                procs.append(p)
                count+=1

        ###################
        # Join forces again
        for p in procs:
            ## Get error messages
            if self.debug>0:
                p.get()
        pool.close()
        pool.join()

        ## Extract results
        for j in range(count):
            i,plane,res,res2 = results[j]
            ind1 = indexes[i]
            ind2 = indexes[i+1]
            if plane==0:
                if np.any(res2!=self.M.Q_freex[ind1:ind2]):
                    print('ERROR in SD_calcSinglePsiParallel: Not same input Q_free as output...')
                    print('  input:  ',self.M.Q_freex[ind1:ind2])
                    print('  output: ',res2)
                self.M.DQ_SDx[ind1:ind2] = res
            else:
                if np.any(res2!=self.M.Q_freey[ind1:ind2]):
                    print('ERROR in SD_calcSinglePsiParallel: Not same input Q_free as output...')
                    print('  input:  ',self.M.Q_freey[ind1:ind2])
                    print('  output: ',res2)
                self.M.DQ_SDy[ind1:ind2] = res
        return

    ###################################################################
    ## Calculate SD for all distributions calculated with the solver ##
    def SD_calcEvolution(self,planes=[0,1],flag_RecalcSD=False,**kwargs):
        """
        kwargs: nQ, indexSD, debug, integrator_epsilon, widthRatioSD
            widthRatioSD: if smaller than 1 - calculate a narrow SD
        """
        start = time.time()

        ## Get keyword arguments
        integrator_epsilon = kwargs.pop('integrator_epsilon',self.integrator_epsilon)
        widthRatioSD = kwargs.pop('widthRatioSD',self.widthRatioSD)
        nQ       = kwargs.pop('nQ',self.nQ)
        debug    = kwargs.pop('debug',self.debug)
        indexSD  = kwargs.pop('indexSD',self.indexSD)
        ntSD     = np.size(indexSD)
        interpOrderPsi = kwargs.pop('interpOrderPsi',self.interpOrderPsi)
        detuningScale = 1
        center_tune=np.nan

        ## Paralellisation preperation
        nProcs = np.min([self.maxCPU,np.size(planes)*np.size(indexSD),(mp.cpu_count()+1)//2])
        if nProcs==0:
            print('WARNING in SD_calcEvolution: No processes needed? nProcs=0')
            return -1
        elif nProcs==1:
            print('SD_calcEvolution: Running without multiprocessing.')
        elif nProcs>1:
            print("SD_calcEvolution: %d processes (ncpus=%d)"%(nProcs,mp.cpu_count()))
            pool = mp.Pool(processes = nProcs)

        def collect_result(res):
            results.append(res)
        count=0
        procs = []
        results = []   # must be after def collect_result...


        ## Start the calculations
        for plane in planes:
            if not flag_RecalcSD:
                if self.flag_storeSDWhileSolving:
                    print('SD_calcEvolution: Stability diagrams stored while solving the PDE - exiting.')
                    return
                if self.flag_SDAllx and plane ==0:
                    print('SD_calcEvolution: Horizontal stability diagrams already calculated.')
                    continue
                if self.flag_SDAlly and plane ==1:
                    print('SD_calcEvolution: Vertical stability diagrams already calculated.')
                    continue
            if plane==0  :
                self.DQ_SDAllx = np.zeros((ntSD,nQ),dtype='complex')
                self.Q_freeAllx = np.zeros((ntSD,nQ))
            elif plane==1:
                self.DQ_SDAlly = np.zeros((ntSD,nQ),dtype='complex')
                self.Q_freeAlly = np.zeros((ntSD,nQ))
            else:
                print('ERROR in SD_calcEvolution: The following plane number does not exist',plane)
                continue

            ## Set the detuning - Assume for now linear detuning
            detuning = [self.M.Q, self.M.Qy][plane]

            ## Commit jobs
            for i,ind in enumerate(indexSD):
                ind = np.argmax(ind==self.indexPSI)
                ## Get the distribution
                if ind<0:
                    distribution=Gaussian()
                else:
                    distribution = self.calcInterpDist(self.psis[ind],plane,flag_BC_gradient_0=True,interpOrderPsi=interpOrderPsi)
                Q_free = self._getQfree(detuning,center_tune,widthRatioSD,plane,distribution=distribution)

                args = [i,plane,distribution,detuning,Q_free,self.integrator_epsilon]
                if nProcs>1:
                    p = pool.apply_async(self._workerSD, args = (args,),callback = collect_result)
                    procs.append(p)
                else:
                    collect_result(self._workerSD(args))
                count+=1

        ###################
        # Join forces again
        if nProcs>1:
            for p in procs:
                ## Get error messages
                if self.debug>0:
                    p.get()
            pool.close()
            pool.join()

        ## Extract results
        for j in range(count):
            i,plane,res,res2 = results[j]
            if plane==0:
                self.DQ_SDAllx[i] = res
                self.Q_freeAllx[i] = res2
                self.flag_SDAllx = True
            else:
                self.DQ_SDAlly[i] = res
                self.Q_freeAlly[i] = res2
                self.flag_SDAlly = True
        print('SD_calcEvolution: Total wall time=%.2fs'%(time.time()-start))
        return #self.DQ_SDAllx,self.Q_freeAllx,self.DQ_SDAlly,self.Q_freeAlly

    #########################################################
    ## Calculate SD for IC with various octupole strengths ##
    def SD_calcScaledPsi0(self,plane=0,ind=0,**kwargs):
        """
        ind =  >=0: With calculated distribution, <0: With Gaussian()
        kwargs: nQ, debug, integrator_epsilon, scales
        """
        start = time.time()
        # Load important parameters
        nQ       = kwargs.pop('nQ',self.nQ)
        debug    = kwargs.pop('debug',self.debug)
        integrator_epsilon = kwargs.pop('integrator_epsilon',self.integrator_epsilon)
        scales   = kwargs.pop('scales',self.scales)
        interpOrderPsi = kwargs.pop('interpOrderPsi',self.interpOrderPsi)

        ## Paralellisation preperation
        nSD_scaled = np.size(scales)
        nProcs = np.min([self.maxCPU,nSD_scaled,(mp.cpu_count()+1)//2])
        if nProcs==0:
            print('WARNING in SD_calcScaledPsi0: No processes needed? nProcs=0')
            return -1
        elif nProcs==1:
            print('SD_calcScaledPsi0: Running without multiprocessing.')
        elif nProcs>1:
            print("SD_calcScaledPsi0: %d processes (ncpus=%d)"%(nProcs,mp.cpu_count()))
            pool = mp.Pool(processes = nProcs)

        def collect_result(res):
            results.append(res)
        count=0
        procs = []
        results = []   # must be after def collect_result...

        # Full width SD
        center_tune=None
        widthRatioSD=1
        if ind<0:
            Gaussian()
        else:
            distribution = self.calcInterpDist(self.psis[ind],plane,flag_BC_gradient_0=True,interpOrderPsi=interpOrderPsi)

        for i, detuningScale in enumerate(np.concatenate((scales,[0.5]))):
           # args = [i,ind,plane,nQ,debug,integrator_epsilon,scale]
            ## Set the detuning - Assume for now linear detuning
            if plane==0: detuning = self.M.Q    # detuning = LinearDetuning(self.M.Q.Q0x, a, b)
            else:        detuning = self.M.Qy   # detuning = LinearDetuning(self.M.Q.Q0y, a, b)
            if detuningScale!=1:
                detuning = copy.deepcopy(detuning)
                detuning.scale(detuningScale)
            Q_free = self._getQfree(detuning,center_tune,widthRatioSD,plane,distribution=distribution)

            # args = [i,ind,plane,interpOrderPsi,nQ,integrator_epsilon*detuningScale,debug,detuningScale,center_tune,widthRatioSD]
            args = [i,plane,distribution,detuning,Q_free,self.integrator_epsilon*detuningScale]
            if nProcs>1:
                p = pool.apply_async(self._workerSD, args = (args,),callback = collect_result)
                procs.append(p)
            else:
                collect_result(self._workerSD(args))
            count+=1

        ###################
        # Join forces again
        if nProcs>1:
            for p in procs:
                ## Get error messages
                if self.debug>0:
                    p.get()
            pool.close()
            pool.join()

        DQ_SD_psi0_scaled=np.zeros((nSD_scaled,nQ),dtype=complex)
        Q_free_psi0_scaled=np.zeros((nSD_scaled,nQ))
        for j in range(count):
            i,_,res1,res2 = results[j]
            if i < nSD_scaled:
                DQ_SD_psi0_scaled[i]=res1
                Q_free_psi0_scaled[i]=res2
            else:
                if plane==0:
                    self.DQ_SD_halfIx=res1
                else:
                    self.DQ_SD_halfIy=res1

        ## Store in self
        if plane ==0:
            self.DQ_SD_psi0_scaledx = DQ_SD_psi0_scaled
            self.Q_free_psi0_scaledx = Q_free_psi0_scaled
            self.scalesx = scales
        else:
            self.DQ_SD_psi0_scaledy = DQ_SD_psi0_scaled
            self.Q_free_psi0_scaledy = Q_free_psi0_scaled
            self.scalesy = scales

        print('SD_calcScaledPsi0: Total wall time=%.2fs'%(time.time()-start))
        return DQ_SD_psi0_scaled,Q_free_psi0_scaled

    def SD_copyScaledPsi0(self,planeFrom):
        if planeFrom==0:
            self.DQ_SD_psi0_scaledy = self.DQ_SD_psi0_scaledx
            self.Q_free_psi0_scaledy = self.Q_free_psi0_scaledx
            self.scalesy = self.scalesx
            self.DQ_SD_halfIy = self.DQ_SD_halfIx
        else:
            self.DQ_SD_psi0_scaledx = self.DQ_SD_psi0_scaledy
            self.Q_free_psi0_scaledx = self.Q_free_psi0_scaledy
            self.scalesx = self.scalesy
            self.DQ_SD_halfIx = self.DQ_SD_halfIy

    #############################
    ## Plot stability diagrams ##
    def SD_plotSD(self,plane,marker='',label=None,**kwargs):
        if plane==0:
            plane_char = 'x'
            DQ_SD = self.M.DQ_SDx
            wmode__DQ = self.M.wmode__DQx
        else:
            plane_char = 'y'
            DQ_SD = self.M.DQ_SDy
            wmode__DQ = self.M.wmode__DQy

        if np.size(wmode__DQ)==0 and np.size(DQ_SD)<=1:
            print('SD_plotSD: No modes or stability diagram to be plotted in plane %s.'%plane_char)
            return

        flag_NormQ = kwargs.pop('flag_NormQ',0)
        if flag_NormQ and self.M.Q.ax!=0:
            Qunit = abs(self.M.Q.ax)
            Qunitlabel=[r' $[a]$',r' $[|a|]$'][self.M.Q.ax<0]
        else:
            Qunit=1
            Qunitlabel = ''

        if 'ax' in kwargs:
            ax = kwargs.get('ax')
            fig = ax.get_figure()
        else:
            fig, ax = plt.subplots(1)

        title = kwargs.get('title','')
        ax.set_title(title)
        ax.plot(DQ_SD.real/Qunit, DQ_SD.imag/Qunit, '-',marker=marker, label=label,c='b')
        ax.plot(wmode__DQ.real/Qunit,wmode__DQ.imag/Qunit,'kx',ms=10,mew=1)

        ax.set_xlabel(self._DQ_coh_R_label+Qunitlabel)
        ax.set_ylabel(self._DQ_coh_I_label+Qunitlabel)
        ax.grid(True)

        minx = np.min(DQ_SD.real/Qunit)
        maxx = np.max(DQ_SD.real/Qunit)
        ax.set_xlim(minx,maxx)
        fig.tight_layout()

        return  fig,ax

    def SD_plotAllSD(self,plane,flag_FillSD=True,flag_SaveFig=False,savedir='01_Plots/',figname='fig',
                  flag_SDConstituents=False,iTime_step=1,flag_PlotModes=False,marker='',plot_interpOrderSD=1,**kwargs):


        flag_SD = [self.flag_SDAllx,self.flag_SDAlly][plane]
        if not(flag_SD):
            print('WARNING in SD_plotAllSD: Have not calculated stability diagram for plane %s'%(['x','y'][plane]))
            return -1

        if plane==0:
            DQ_SDAll = self.DQ_SDAllx
            DQ_freeAll = self.Q_freeAllx - self.M.Q.Q0x
            plane_char = 'x'
            wmode__DQ = self.M.wmode__DQx
        else:
            DQ_SDAll = self.DQ_SDAlly
            DQ_freeAll = self.Q_freeAlly - self.M.Q.Q0y
            plane_char = 'y'
            wmode__DQ = self.M.wmode__DQy


        time_scale = self.time_scale
        tlabel= self.tlabel
        ts = self.tsSD
        tmax = kwargs.pop('tmax',self.tmax)
        ts = ts[ts<=tmax]
        flag_NormQ = kwargs.pop('flag_NormQ',0)
        if flag_NormQ and self.M.Q.ax!=0:
            Qunit = abs(self.M.Q.ax)
            Qunitlabel=[r' $[a]$',r' $[|a|]$'][self.M.Q.ax<0]
        else:
            Qunit=1
            Qunitlabel = ''

        if 'ax' in kwargs:
            ax = kwargs.get('ax')
            fig = ax.get_figure()
        else:
            fig, ax = plt.subplots(1)#, figsize=(16, 15))
        # col = sns.color_palette("husl", len(tsODE), 0.6)

        # Set colorbar as time measurement:
        cmap = cm.get_cmap(None)
        if flag_FillSD:
            bounds = ts/time_scale
            if np.size(bounds)==2:
                bounds = np.array([bounds[0],np.mean(bounds),bounds[1]])
            if tmax>np.max(ts):
                bounds = np.concatenate((ts,[tmax]))/time_scale
        else:
            bounds = np.linspace(ts[0],tmax,256)

        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
        cmap = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)
        cmap.set_array([])

        if 0:
            # Stopped trying this...
            normed = (np.array(bounds)-bounds[0])/(bounds[-1]-bounds[0])
            listOfCol = [cmap((normed[i]+normed[i+1])*.5) for i in range(len(bounds)-1) ]
            norm= mpl.colors.BoundaryNorm(boundaries=bounds, ncolors=len(listOfCol))
            cmap = mpl.colors.ListedColormap(listOfCol)

       # norm = mpl.colors.Normalize(vmin=ts.min()/time_scale, vmax=tmax/time_scale)
       # nColor = int(tmax/ts[1]-0.5)
       # cmap = mpl.cm.ScalarMappable(norm=norm, cmap=cm.get_cmap(None,lut=[None,nColor][flag_FillSD]))



        for i,t in enumerate(ts):
            if i%iTime_step>0: continue
            if flag_FillSD:
                if i==0: continue
                ax.fill(np.concatenate((DQ_SDAll[i].real/Qunit,DQ_SDAll[i-iTime_step][::-1].real/Qunit)),
                        np.concatenate((DQ_SDAll[i].imag/Qunit,DQ_SDAll[i-iTime_step][::-1].imag/Qunit)),
                        color=cmap.to_rgba(0.5*(t+ts[i-iTime_step])/time_scale))
            else :
                DQ_SD_R = DQ_SDAll[i].real
                DQ_SD_I = DQ_SDAll[i].imag
                if plot_interpOrderSD==1:
                    ax.plot(DQ_SD_R/Qunit, DQ_SD_I/Qunit, '-',marker=marker, c=cmap.to_rgba(t/time_scale))
                else:
                    kind = ['nearest','linear','quadratic','cubic'][plot_interpOrderSD]
                    DQ_Free = DQ_freeAll[i]
                    interp_SD=sc.interpolate.interp1d(DQ_Free, np.array([DQ_SD_R,DQ_SD_I]),
                        kind=kind, axis=1,bounds_error=False,fill_value=0)
                    DQ_Free_interp = np.linspace(DQ_Free[0],DQ_Free[-1],10**5)
                    DQ_SD_R_interp,DQ_SD_I_interp = interp_SD(DQ_Free_interp)
                    ax.plot(DQ_SD_R/Qunit, DQ_SD_I/Qunit, marker=marker, ls='',c=cmap.to_rgba(t/time_scale))
                    ax.plot(DQ_SD_R_interp/Qunit, DQ_SD_I_interp/Qunit, '-',marker=marker, c=cmap.to_rgba(t/time_scale))

        if flag_PlotModes:
            ax.plot(wmode__DQ.real/Qunit,wmode__DQ.imag/Qunit,'kx',ms=10,mew=1)


        ax.set_xlabel(self._DQ_coh_R_label+Qunitlabel)
        ax.set_ylabel(self._DQ_coh_I_label+Qunitlabel)

        ax.grid(True)

        minx = np.min(DQ_SDAll[0].real/Qunit)
        maxx = np.max(DQ_SDAll[0].real/Qunit)
        ax.set_xlim(minx,maxx)

        # Finish colorbar
        fac=1.4
        if not 'ax'in kwargs:
            cb=fig.colorbar(cmap,label=tlabel,fraction=0.046/fac,aspect=20*fac, pad=0.02,
                            boundaries=bounds,spacing='proportional') #ticks = np.linspace(0,tmax/time_scale,4),norm=norm,
            tick_locator = mpl.ticker.MaxNLocator(nbins=5)
            cb.locator = tick_locator
            cb.update_ticks()
            fig.tight_layout()

        if flag_SaveFig: fig.savefig(savedir+'%s.eps'%(figname))

        ####################################################################################
        ####################################################################################
        if flag_SDConstituents:
            absax = abs(self.M.Q.ax)

            fig2,ax2 =plt.subplots(2,1,sharex=True)
            # Set colorbar as time measurement:
            norm = mpl.colors.Normalize(vmin=ts.min()/time_scale, vmax=ts.max()/time_scale)
            cmap = mpl.cm.ScalarMappable(norm=norm, cmap=cm.get_cmap(None))
            cmap.set_array([])

            for i,t in enumerate(ts):
                DQ =  DQ_freeAll[i]
                ax2[0].plot(DQ/absax,DQ_SDAll[i].real-DQ, c=cmap.to_rgba(t/time_scale))
                ax2[1].plot(DQ/absax,DQ_SDAll[i].imag, c=cmap.to_rgba(t/time_scale))
            ax2[0].set_ylabel(r'Real $\Delta Q_\mathrm{SD} - \Delta Q_\mathrm{free}$')
            ax2[1].set_ylabel(r'Imag $\Delta Q_\mathrm{SD}$')
            ax2[1].set_xlabel(r'$Q-Q_0$ $[|a|]$')
            for i in range(2):
                ax2[i].grid(True)
            fig2.tight_layout()
            fig2.subplots_adjust(hspace=0)

        ####################################################################################
        ####################################################################################
            fig2,ax2 =plt.subplots(2,1,sharex=True)
            # Set colorbar as time measurement:
            norm = mpl.colors.Normalize(vmin=ts.min()/time_scale, vmax=ts.max()/time_scale)
            cmap = mpl.cm.ScalarMappable(norm=norm, cmap=cm.get_cmap(None))
            cmap.set_array([])

            for i,t in enumerate(ts):
                DQ =  DQ_freeAll[i]
                ReDQ = DQ_SDAll[i].real
                ImDQ = DQ_SDAll[i].imag
                ReInt= -ReDQ / (ReDQ**2 + ImDQ**2)
                ImInt=  ImDQ / (ReDQ**2 + ImDQ**2)
                ax2[0].plot(DQ/absax, ReInt, c=cmap.to_rgba(t/time_scale))
                ax2[1].plot(DQ/absax, ImInt, c=cmap.to_rgba(t/time_scale))
            ax2[0].set_ylabel('Real integral')
            ax2[1].set_ylabel('Imag integral')
            ax2[1].set_xlabel(r'$Q-Q_0$ $[|a|]$')
            for i in range(2):
                ax2[i].grid(True)
            fig2.tight_layout()
            fig2.subplots_adjust(hspace=0)


        return fig,ax

    def SD_plotWaterfall(self,plane,basex=None,nlevels=30,flag_LogScale=0):

        flag_SD = [self.flag_SDAllx,self.flag_SDAlly][plane]
        if not(flag_SD):
            print('WARNING in SD_plotAllSD: Have not calculated stability diagram for plane %s'%(['x','y'][plane]))
            return -1

        ## Import necessary values
        if plane==0:
            DQ_SDAll = self.DQ_SDAllx
        else:
            DQ_SDAll = self.DQ_SDAlly
        time_scale = self.time_scale
        tlabel= self.tlabel
        ts = self.tsSD
        if basex is None:
            minx = np.min(DQ_SDAll[0].real)
            maxx = np.max(DQ_SDAll[0].real)
            basex = np.linspace(minx,maxx,100)


        # Interpolate each SD to a uniform grid
        nt = np.size(ts)
        nx = np.size(basex)
        AllY = np.zeros((nt,nx))
        for i in range(nt):
            x = DQ_SDAll[i].real
            y = DQ_SDAll[i].imag
            kind = ['nearest','linear','quadratic','cubic'][self.interpOrderSD]
            f = sc.interpolate.interp1d(x,y,kind=kind,bounds_error=False,fill_value=0)
            AllY[i] = f(basex)

        # Find how to show plot
        lims = [0,min(np.max(AllY),2.*np.max(AllY[:,int(nx/2)]))]
        norm = colors.Normalize(vmin = lims[0],vmax=lims[1],clip=False)
        cticks=None
        if np.max(AllY)>lims[1] or flag_LogScale :
            exp = int(101.5-np.log10(lims[1]) )-100
            thresh = np.round(lims[1],decimals=exp)
            ratio = 1+np.int(np.log10(np.max(AllY)/thresh))
            nDec = min(2,ratio)

            norm=colors.SymLogNorm(vmax=thresh*10**nDec,vmin=0,linthresh=thresh,linscale=1,clip=False,base=10)
            cticks = np.concatenate(([0,np.round(thresh/2,decimals=exp)],thresh*np.logspace(0,nDec,nDec+1)))
        levels=norm.inverse(np.linspace(0,1,nlevels))

        # Create figure
        fig = plt.figure()
        ax=fig.gca()
        cmap = cm.get_cmap(None)
        plt.contourf(AllY,cmap=cmap,levels=levels,norm=norm,
                     extent=(np.min(basex),np.max(basex),0,np.max(ts)/time_scale),extend='both')
        cb = plt.colorbar(label = self._DQ_coh_I_label,#extend='both',
                          ticks=cticks)

        # Set Cticklabels
        if (cticks is None):
            tick_locator = mpl.ticker.MaxNLocator(nbins=5)
            cb.locator = tick_locator
            cb.update_ticks()
        else:
            labels = [item.get_text() for item in cb.ax.get_yticklabels()]
            for i in range(len(labels)):
                labels[i] = PyRADISE.Plotting.sci_not(cticks[i],0,flag_Ignore1=True)
            cb.ax.set_yticklabels(labels)

        # Fix rest of layout
        ax.set_xlabel(self._DQ_coh_R_label)#, fontsize=20)
        ax.set_ylabel(tlabel)
        fig.tight_layout()

        return fig,ax

    ##############################
    ## Damping rate of one mode ##

    def SD_calcGrowthRateOneModeOnePsi(self,Q0,DQ_mCoh,distribution,detuning,
                                        relstep=[.5,.05],tol=1e-4,taylorAlphaOld=0+0j,**kwargs):
        debug = kwargs.pop('debug',self.debug)
        flag_FindAlpha = kwargs.pop('flag_FindAlpha',self.flag_FindAlpha)

        ## Set the integrator - Only set 1 to reduce memory usage
        integrator1 = Integrator(distribution, detuning, maxJ=self.JMaxSD,epsilon=self.integrator_epsilon*1,nStep=self.nStep)

        return PyRADISE.Coefficients.calc_LandauDampedOneMode(self.integrator_epsilon,integrator1,
                                            Q0,DQ_mCoh,relstep=relstep,tol=tol,debug=debug,
                                            flag_FindAlpha=flag_FindAlpha,flag_UpdateAlpha=self.flag_UpdateAlpha,
                                            taylorAlphaOld=taylorAlphaOld)

    def SD_calcGrowthRate(self,plane=0,DQ_mCoh = 0 + 0*1j,**kwargs):
        if plane==0:
            Q0   = self.M.Q.Q0x
            detuning = self.M.Q
        else:
            Q0   = self.M.Q.Q0y
            detuning = self.M.Qy

        growthRate = np.zeros(self.ntPSI)
        DQ_mLD = 0+0j
        taylorAlpha=0+0j

        # Calculate stability margin
        for it in range(self.ntPSI):
            distribution = self.calcInterpDist(self.psis[it],plane=plane,flag_BC_gradient_0=True)

            DQ_mLDOld=DQ_mLD
            DQ_mLD,DQ_mFree,DQ_mSD,cnt,taylorAlpha, relerr = self.SD_calcGrowthRateOneModeOnePsi(Q0,DQ_mCoh,distribution,detuning,
                                                                                    taylorAlphaOld=taylorAlpha,**kwargs)

            if not(self.flag_UpdateReQ or np.abs(DQ_mLDOld)==0):
                DQ_mLD = DQ_mLDOld.real + 1j*DQ_mLD.imag

            print('SD_calcGrowthRate: Mode(t=%.2f %s): %.2e %s%.2ej -> %.2e %s%.2ej (relerr(%d iterations)=%.1e)'%(
                self.tsPSI[it]/self.time_scale,self.tunit,
                DQ_mCoh.real,['+','-'][np.sign(DQ_mCoh.imag)<0],np.abs(DQ_mCoh.imag),
                DQ_mLD.real,['+','-'][np.sign(DQ_mLD.imag)<0],np.abs(DQ_mLD.imag),cnt,relerr))
            growthRate[it] = DQ_mLD.imag

        ## Report if unstable
        if np.any(growthRate>0):
            ind = np.argmax(growthRate>0)-1
            if ind>=0:
                tsGR= self.tsPSI
                lat = tsGR[ind]-growthRate[ind]/(growthRate[ind+1]-growthRate[ind])*(tsGR[ind+1]-tsGR[ind])
                print('SD_calcGrowthRate: Latency (%s) = %.2f %s  (the official numerical latency is calculated based on the stabilityMargin)'%(
                            ['x','y'][plane],lat/self.time_scale,self.tunit))

        ## Store it
        if plane==0:
            self.wmodeTestx = DQ_mCoh
            self.growthRatex = growthRate
            self.indexGRx = self.indexPSI
        else:
            self.wmodeTesty = DQ_mCoh
            self.growthRatey = growthRate
            self.indexGRy = self.indexPSI
        return

    def SD_plotGrowthRate(self,plane=0,flag_Normalize=1,label='',c=None,marker='o',**kwargs):
        """
        kwargs include: ax, iTime_step
        """
        try:
            if plane==0:
                growthRate=self.growthRatex
                indexGR = self.indexGRx
            else:
                growthRate=self.growthRatey
                indexGR = self.indexGRy
        except:
            print('WARNING in SD_plotGrowthRate: Could not find attributes in plane %s'%['x','y'][plane])
            return

        if 'ax' in kwargs:
            ax1 = kwargs.pop('ax')
            ax1.autoscale(axis='y')  # Necessary to update the ylims with the new data!
            fig1= ax1.get_figure()
        else:
            fig1,ax1=plt.subplots(1)

        string_normalize= ''
        labelapp=''

        if flag_Normalize:
            growthRate0 = growthRate[0]
            growthRate = growthRate/np.abs(growthRate0)
            string_normalize='Rel. '

        # Find how many GR we have.
        index = indexGR
        tsGR= self.tsODE[index]
        ax1.plot(tsGR/self.time_scale,growthRate,'-',marker=marker,c=c,label=label+labelapp)
        ax1.set_ylabel(r'%s'%(string_normalize)+self._DQ_LD_I_label)

        # Layout
        ax1.set_ylim(top=0)
        ax1.set_xlabel(self.tlabel)
        ax1.grid(True)
        if not label=='':
            ax1.legend(loc=0)
        fig1.tight_layout()

        # Report if unstable
        if np.any(growthRate>0):
            ind = np.argmax(growthRate>0)-1
            if ind>=0:
                lat = tsGR[ind]-growthRate[ind]/(growthRate[ind+1]-growthRate[ind])*(tsGR[ind+1]-tsGR[ind])
                print('SD_plotGrowthRate: Latency (%s) = %.2f %s  (the official numerical latency is calculated based on the stabilityMargin)'%(
                            ['x','y'][plane],lat/self.time_scale,self.tunit))

        return fig1,ax1

    ################################
    ## Margin in damping of modes ##
    def SD_calcStabilityMarginAllModesOneSD(self,plane,DQ_SD,wmode__DQ = None):
        """
        Interpolate the stability diagram (DQ_SD) over all stretches where DQ_SD.real increases (in case of loops).
        Check that all modes wmode__DQ are below the interpolated function: wmode__DQ.imag < func(wmode__DQ.real)
        Can also be given a list (1+) of wmodes, instead of using the wmodes used to calculate Psi(t)
        """
        ## 1: Load relevant modes
        if wmode__DQ==None:
            if [self.M.nWmodex,self.M.nWmodey][plane]==0:
                print('SD_calcStabilityMarginAllModesOneSD: No modes in plane %s - Cannot be unstable.'%(['x','y'][plane]))
                return np.nan, np.nan, np.nan
            if plane==0:
                wmode__DQ = copy.copy(self.M.wmode__DQx)
            elif plane==1:
                wmode__DQ = copy.copy(self.M.wmode__DQy)
        else:
            if np.shape(wmode__DQ)==():
                wmode__DQ = [wmode__DQ]
            wmode__DQ = np.array(wmode__DQ)

        ## Only check stability of modes that can go unstable
        wmode__DQ[wmode__DQ.imag<=0] -= 1e9*1j   # became nan when subtracting np.inf*1j

        if np.max(np.imag(wmode__DQ))<0:
            print('SD_calcStabilityMarginAllModesOneSD: No modes in plane %s require Landau Damping - Cannot be unstable.'%(['x','y'][plane]))
            return np.nan,np.nan,np.nan

        ## 2: Interpolate stability diagram in multiple areas
        funcs = self._interpSD(DQ_SD)
        stabilityMargins = np.inf*np.ones(np.size(wmode__DQ))
        stabilityMargin  = np.inf
        indLeastStable = 0

        for func in funcs:
            ## 3: Check imaginary distance from SD to modes (should be stable)
            newMargins = func(wmode__DQ.real)-wmode__DQ.imag
            index = (newMargins<stabilityMargins)
            stabilityMargins[index] = newMargins[index]

            if np.min(newMargins)<stabilityMargin:
                indLeastStable = np.argmin(newMargins)
                stabilityMargin = np.min(newMargins)

        return stabilityMargins,stabilityMargin,indLeastStable,wmode__DQ[indLeastStable]

    def SD_calcStabilityMargin(self,plane=0,wmode__DQ=None,debug=0):
        """
        Old approach (for a single mode and all times)
        """
        if not [self.flag_SDAllx,self.flag_SDAlly][plane]:
            print('WARNING in SD_calcStabilityMargin: Have not calculated SD%s'%['x','y'][plane])
            return

        if plane==0:
            if not np.all(self.stabilityMarginx==0):
                print('SD_calcStabilityMargin: Stability margin in %s calculated when solving the PDE - Exiting.'%(['x','y'][plane]))
                return
            DQ_SDAll = self.DQ_SDAllx
        else:
            if not np.all(self.stabilityMarginy==0):
                print('SD_calcStabilityMargin: Stability margin in %s calculated when solving the PDE - Exiting.'%(['x','y'][plane]))
                return
            DQ_SDAll = self.DQ_SDAlly

        stabilityMargin = np.zeros(self.ntSD)
        indLeastStable  = np.zeros(self.ntSD,dtype=np.int32)

        for iTime in range(self.ntSD):
            DQ_SD = DQ_SDAll[iTime]
            _,stabilityMargin[iTime],indLeastStable[iTime],wmodeTest = self.SD_calcStabilityMarginAllModesOneSD(plane,DQ_SD,wmode__DQ=wmode__DQ)

        if plane==0:
            self.wmodeTestx = wmodeTest
            self.stabilityMarginx = stabilityMargin
            self.indLeastStablex = indLeastStable
            self.indexSMx = self.indexSD
        else:
            self.wmodeTesty = wmodeTest
            self.stabilityMarginy = stabilityMargin
            self.indLeastStabley = indLeastStable
            self.indexSMy = self.indexSD
        return

    def SD_plotStabilityMargin(self,plane=0,flag_Relative=1,flag_Normalize=1,label='',c=None,
                                flag_PlotInscribed=False,**kwargs):
        """
        kwargs include: ax, iTime_step,ylabel
        """
        try:
            if plane==0:
                try:
                    DQ_mCoh = self.wmodeTestx
                except:
                    DQ_mCoh = self.M.wmode__DQx[self.leastStablex[-1]]
                stabilityMargin=self.stabilityMarginx
                indexSM = self.indexSMx
            else:
                try:
                    DQ_mCoh = self.wmodeTesty
                except:
                    DQ_mCoh = self.M.wmode__DQy[self.leastStabley[-1]]
                stabilityMargin=self.stabilityMarginy
                indexSM = self.indexSMy
        except:
            print('WARNING in SD_plotStabilityMargin: Could not find attributes in plane %s'%['x','y'][plane])
            return

        if 'ax' in kwargs:
            ax1 = kwargs.pop('ax')
            ax1.autoscale(axis='y')  # Necessary to update the ylims with the new data!
            fig1= ax1.get_figure()
        else:
            fig1,ax1=plt.subplots(1)

        string_relative = ' - \Delta Q_\mathrm{mode}'
        string_normalize= ''
        labelapp=''
        if not flag_Relative:
            stabilityMargin = stabilityMargin + DQ_mCoh.imag
            string_relative = ''
            labelapp = ' thr=%.1e'%DQ_mCoh.imag

        if flag_Normalize:
            margin0 = stabilityMargin[0]
            stabilityMargin = stabilityMargin/margin0
            string_normalize='Rel. '
            if len(labelapp)>0:
                labelapp = ' thr=%.2f'%(DQ_mCoh.imag/margin0)

        ax1.plot(self.tsODE[indexSM]/self.time_scale,stabilityMargin,'o-',c=c,label=label+labelapp)
        ylabel = kwargs.get('ylabel',r'%s$\operatorname{Im}\{ \Delta Q_\mathrm{SD}%s\}$'%(string_normalize,string_relative))
        ax1.set_ylabel(ylabel)

        ## Layout
        ax1.set_ylim(bottom=0)
        ax1.set_xlabel(self.tlabel)
        ax1.grid(True)
        if not label=='':
            ax1.legend(loc=0)
        fig1.tight_layout()

        ## Plot mode in stability diagram
        if flag_PlotInscribed:
            iTime_step = kwargs.pop('iTime_step',1)
            flag_NormQ = kwargs.get('flag_NormQ',0)
            if flag_NormQ and self.M.Q.ax!=0:
                Qunit = abs(self.M.Q.ax)
            else:
                Qunit=1
            fig2,ax2=self.SD_plotAllSD(plane=plane,iTime_step=iTime_step,**kwargs)

            ax2.plot(DQ_mCoh.real/Qunit,DQ_mCoh.imag/Qunit,'kx',ms=10,mew=2)
            return fig1,ax1,fig2,ax2
        return fig1,ax1


    ####################################################
    ## Relative change of effective detuning strength ##
    def SD_calcEffectiveStrength(self,plane=0,flag_InterpStrength = 1,
                                 DQ_mCoh_R = 0,flag_AllReDQ = 1,maxmin_ratio=np.nan,**kwargs):

        ## Get keyword arguments
        interpOrderSD = kwargs.pop('interpOrderSD',self.interpOrderSD)
        debug = kwargs.pop('debug',self.debug)


        if not [self.flag_SDAllx,self.flag_SDAlly][plane]:
            print('WARNING in SD_calcEffectiveStrength: Have not calculated SD%s'%['x','y'][plane])
            return
        try:
            if plane==0:
                scales = self.scalesx
                DQ_SDAll = self.DQ_SDAllx
                Q_freeAll = self.Q_freeAllx
                DQ_SD_psi0_scaled = self.DQ_SD_psi0_scaledx
                Q_free_psi0_scaled = self.Q_free_psi0_scaledx

            else:
                scales = self.scalesy
                DQ_SDAll = self.DQ_SDAlly
                Q_freeAll = self.Q_freeAlly
                DQ_SD_psi0_scaled = self.DQ_SD_psi0_scaledy
                Q_free_psi0_scaled = self.Q_free_psi0_scaledy
        except:
            print('WARNING in SD_calcEffectiveStrength: Could not find attributes in plane %s'%['x','y'][plane])
            return

        kind = ['nearest','linear','quadratic','cubic'][interpOrderSD]
        nSD_scaled = np.size(scales)
        effectiveStrength = np.zeros(self.ntSD)

        for i in range(self.ntSD):
            Q_free = Q_freeAll[i]
            DQ_SD = DQ_SDAll[i]

            DQ_SD_R = np.real(DQ_SD)
            DQ_SD_I = np.imag(DQ_SD)

            ## Find test points
            if flag_AllReDQ:
                ## 1: Interpolate stability diagram
                interp_SD=sc.interpolate.interp1d(Q_free, np.array([DQ_SD_R,DQ_SD_I]),
                    kind=kind, axis=1,bounds_error=False,fill_value=0)

                ## 2: Find many points along the stability diagram
                DQ_R_test,DQ_I_test = interp_SD(np.linspace(Q_free[0],Q_free[-1],10000))

                ## 3: Discard points on the stability diagram with small imaginary part (typically at the edges)
                if np.isnan(maxmin_ratio):
                    ind = DQ_R_test*0<1
                else:
                    ind = np.abs(DQ_I_test) > np.max(DQ_I_test)/maxmin_ratio

                DQ_R_test = DQ_R_test[ind]
                DQ_I_test = DQ_I_test[ind]
            else:
                DQ_R_test = DQ_mCoh_R

                ## 1: Interpolate stability diagram
                funcs = self._interpSD(DQ_SD)

                ## 2: Find lowest imaginary part of the stability diagram at given real part
                DQ_I_test = np.inf
                for func in funcs:
                    DQ_I_test = min(DQ_I_test,func(DQ_R_test))

            for j in range(nSD_scaled):
                interp_G  = sc.interpolate.interp1d(DQ_SD_psi0_scaled[j].real,DQ_SD_psi0_scaled[j].imag,
                                                    kind=kind,bounds_error=False,fill_value=0)

                ## Calculate margin, from test modes down to scaled SD
                newmargin = np.min(DQ_I_test-interp_G(DQ_R_test))

                ## If margin is positive, the scaled SD is completely inside the test modes
                if newmargin>=0:
                    indtest = DQ_R_test<-1e-5

                    effectiveStrength[i] = scales[j]
                    ## if interpolate between the calculated strengths
                    if j>0 and flag_InterpStrength:
                        ds = (scales[j]-scales[j-1])
                        dm = (newmargin-oldmargin)
                        effectiveStrength[i] = scales[j] - newmargin * ds/dm
                        if debug>0:
                            print('DEBUG in SD_calcEffectiveStrength: interpolated strength %.2e -> %.2e, ds=%.2e, dm=%.2e'%(scales[j],effectiveStrength[i],ds,dm))
                    break
                oldmargin = newmargin

                ## if does not find a margin
                if j==nSD_scaled-1:
                    effectiveStrength[i]=0
                    print("WARNING in SD_calcEffectiveStrength: Have not calulated a SD with a weak enough scaled strength!")

        if plane==0:
            self.effectiveStrengthx=effectiveStrength #/ effectiveStrength[0]
        else:
            self.effectiveStrengthy=effectiveStrength #/ effectiveStrength[0]
        return effectiveStrength

    def SD_plotEffectiveStrength(self,plane=0,label='',c=None,flag_PlotInscribed=False,**kwargs):
        """
        kwargs include: ax, iTime_step,fmt_string,minIntensity
        """
        fmt_string = kwargs.pop('fmt_string','o-')
        minIntensity = kwargs.pop('minIntensity',0)
        ind = self.moments_x[:,0]>minIntensity
        try:
            if plane==0:
                effectiveStrength=self.effectiveStrengthx

            else:
                effectiveStrength=self.effectiveStrengthy

        except:
            print('WARNING in SD_plotEffectiveStrength: Could not find attributes in plane %s'%['x','y'][plane])
            return

        ## Possibly load given axes
        if 'ax' in kwargs:
            ax1 = kwargs.get('ax')
            ax1.autoscale(axis='y')  # Necessary to update the ylims with the new data!
            fig1= ax1.get_figure()
        else:
            fig1,ax1=plt.subplots(1)

        ax1.plot(self.tsSD[ind]/self.time_scale,effectiveStrength[ind]/effectiveStrength[0],fmt_string,c=c,label=label)

        ## Layout
        ax1.set_ylabel(r'Rel. effective detuning')
        ax1.set_ylim(bottom=0)
        ax1.set_xlabel(self.tlabel)
        ax1.grid(True)
        if not label=='':
            ax1.legend(loc=0)

        if not 'ax' in kwargs:
            fig1.tight_layout()

        ## Plot inscribed SD with scaled strength
        if flag_PlotInscribed:
            iTime_step = kwargs.pop('iTime_step',1)
            fig2,ax2=self.SD_plotAllSD(plane=plane,iTime_step=iTime_step,**kwargs)
            if plane==0:
                DQ_SD_psi0_scaled = self.DQ_SD_psi0_scaledx
                scales = self.scalesx
            else:
                DQ_SD_psi0_scaled = self.DQ_SD_psi0_scaledy
                scales = self.scalesy

            cmap = cm.get_cmap(None)
            for i in range(self.ntSD):
                if i%iTime_step==0:
                    iScale = np.searchsorted(-scales,-effectiveStrength[i],'left',)
                    if iScale==np.size(scales):
                        continue
                    print(r'SD_plotEffectiveStrength: %s(t=%.2e %s): effectiveScale=%.3f , plotScale=%.3f (iScale=%d)'%(
                            ['H','V'][plane],self.tsSD[i]/3600,'h',
                            effectiveStrength[i],scales[iScale],iScale))
                    if iScale<np.size(scales):
                        ax2.plot(DQ_SD_psi0_scaled[iScale].real,DQ_SD_psi0_scaled[iScale].imag,
                                 '--',lw=2,c=cmap((i)/(self.ntSD-1)))
            return fig1,ax1,fig2,ax2
        return fig1,ax1
