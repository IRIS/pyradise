import numpy as np
import scipy as sc
from PySSD.Integrator import Integrator #FixedTrapezoidalIntegrator, SimpsonIntegrator, TrapzIntegrator, DblquadIntegrator
import PyRADISE

#########################
### Physics functions ###
#########################
pi2 = 2*np.pi

def calc_LandauDampedOneMode(integrator_epsilon,integrator1,wmodeQ0,DQ_mCoh,relstep=[.5,.05],tol=1e-4,
                            flag_FindAlpha=[1,1],flag_UpdateAlpha=1,taylorAlphaOld=0,debug=0):
    ## Absolute value of undamped mode tune shift - used to find accuracy of mode
    absDQ_mCoh = np.abs(DQ_mCoh)

    ## Estimate of damped mode tune shift corresponding to free mode
    DQ_mLD = DQ_mCoh.real + 1j*integrator_epsilon

    ## For debugging purposes
    DQ_mLDs=[]
    DQ_mCohs=[]

    cntMax=50
    cnt = 0     # to count number of iterations
    err = 0     # error

    while True and absDQ_mCoh>0:
        if DQ_mLD.imag<=integrator_epsilon*1:
            ## Taylor approach to getting stabilized tune shift
            # tempDQ = 2*integrator1.integrate(wmodeQ0+DQ_mLD.real) - 1*integrator2.integrate(wmodeQ0+DQ_mLD.real) + 1j*(DQ_mLD.imag)
            # tempDQ =(  2*integrator1.integrate(wmodeQ0+DQ_mLD.real)
            #          - 1*integrator1.integrate(wmodeQ0+DQ_mLD.real+1j*integrator_epsilon) + 1j*(DQ_mLD.imag))
            tempDQ = PyRADISE.StabilityDiagram.calc_Stability_Diagram_OneQ(integrator1,integrator_epsilon,wmodeQ0+DQ_mLD.real) + 1j*(DQ_mLD.imag)
            flag_Taylor=True
        else:
            ## Standard approach to getting unstable tune shift
            tempDQ=integrator1.integrate(wmodeQ0+DQ_mLD.real + 1j*DQ_mLD.imag)
            flag_Taylor=False

        ## Calculate error
        errold= err
        err   = tempDQ-DQ_mCoh

        ## Break if within tolerance
        if np.abs(err)<absDQ_mCoh*tol:
            break

        ## Update DQ_mLD (damped mode tune)
        if cnt==0:
            ## Simple method
            dDQ_mLD  =-(err*relstep[0]+errold*relstep[1])
        else:
            ## Newton's method
            dLDDQd__DQ = (dDQ_mLD)/(err-errold)
            dDQ_mLD = -err*dLDDQd__DQ * 0.9**(cnt//10)
        DQ_mLD = DQ_mLD + dDQ_mLD

        ## Debug
        if debug>1 and cnt>cntMax-10:
            print("calc_LandauDampedOneMode: %2d: %11.4e + %11.4ei | %10.2e + %10.2ei - relerr=%.1e"%(
            cnt,DQ_mLD.real,DQ_mLD.imag,err.real,err.imag,np.abs(err)/absDQ_mCoh))

        ## Do not go beyond cntMax steps. If so, take the average of the last 10 values
        if cnt>cntMax-10 :
            DQ_mCohs+=[tempDQ]
            DQ_mLDs+=[DQ_mLD]

            ## Break if tried cntMax times
            if cnt>=cntMax:
                DQ_mLD = np.mean(DQ_mLDs)
                break

        cnt+=1

        ## Reduce relstep ... Not used for cnt>0...
        if cnt%10==0:
            relstep=[relstep[0]*0.9,relstep[1]*0.9]

    ## Keep other tunes - Must be done before calculating alpha!
    DQ_mFree = DQ_mLD.real
    DQ_mSD = DQ_mCoh - 1j*DQ_mLD.imag

    ## Calculate taylorAlpha
    taylorAlpha=1
    if np.any(np.abs(flag_FindAlpha)>0) and flag_Taylor:
        if (flag_UpdateAlpha or abs(taylorAlphaOld)==0):
            # taylorAlpha =  2j*integrator_epsilon/(
            #             integrator4.integrate(wmodeQ0+DQ_mLD.real)-integrator2.integrate(wmodeQ0+DQ_mLD.real))
            taylorAlpha =  2j*integrator_epsilon/( integrator1.integrate(wmodeQ0+DQ_mLD.real +3j*integrator_epsilon)
                                                  -integrator1.integrate(wmodeQ0+DQ_mLD.real +1j*integrator_epsilon))
            taylorAlpha = [1,taylorAlpha.real][flag_FindAlpha[0]>0] + 1j*taylorAlpha.imag*flag_FindAlpha[1]
            if flag_FindAlpha[0]==-1:
                taylorAlpha = -1/taylorAlpha.real + 1j*taylorAlpha.imag
        else:
            taylorAlpha=taylorAlphaOld
        DQ_mLDold = DQ_mLD
        DQ_mLD = DQ_mLD.real + 1j*taylorAlpha*DQ_mLD.imag
        if debug>0:
            print('calc_LandauDampedOneMode: Found taylorAlpha!=1, taylorAlpha=%.2e %s%.2ej |  DQ_mLD = %.2e %s%.3ej -> %.2e %s%.3ej'%(
                        taylorAlpha.real,['+','-'][np.sign(taylorAlpha.imag)<0],np.abs(taylorAlpha.imag),
                        DQ_mLDold.real,['+','-'][np.sign(DQ_mLDold.imag)<0],np.abs(DQ_mLDold.imag),
                        DQ_mLD.real,['+','-'][np.sign(DQ_mLD.imag)<0],np.abs(DQ_mLD.imag)))

    if debug>1 :
        DQ_mCohs = np.array(DQ_mCohs)
        DQ_mLDs = np.array(DQ_mLDs[:-1])
        print('calc_LandauDampedOneMode: Arrays of iterative process of finding the modes. First undamped, secondly damped.',DQ_mCohs,DQ_mLDs)
    return DQ_mLD , DQ_mFree, DQ_mSD, cnt, taylorAlpha, np.abs(err)/absDQ_mCoh

def calc_LandauDampedAllModes(self,plane=0,relstep=[.5,.05],tol=1e-4,flag_FindAlpha=[1,1],debug=0,
                            flag_UpdateReQ=1,flag_UpdateAlpha=1):

    if plane==0:
        wmode__DQ = self.M.wmode__DQx
        wmodeLDDQ = self.M.wmodeLDDQx
        wmodeSDDQ = self.M.wmodeSDDQx
        wmodeQ0   = self.M.Q.Q0x
        distribution=self.interpDistx
        detuning = self.M.Q
        taylorAlphas = self.M.taylorAlphax
    else:
        wmode__DQ = self.M.wmode__DQy
        wmodeLDDQ = self.M.wmodeLDDQy
        wmodeSDDQ = self.M.wmodeSDDQy
        wmodeQ0   = self.M.Q.Q0x
        distribution=self.interpDisty
        detuning = self.M.Qy
        taylorAlphas = self.M.taylorAlphay

    ## Set the integrator - Only set 1 to reduce memory usage
    integrator1 = Integrator(distribution, detuning, maxJ=self.JMaxSD,epsilon=self.integrator_epsilon*1,nStep=self.nStep)
    # integrator2 = Integrator(distribution, detuning, maxJ=self.JMaxSD,epsilon=self.integrator_epsilon*2,nStep=self.nStep)
    # integrator4 = Integrator(distribution, detuning, maxJ=self.JMaxSD,epsilon=self.integrator_epsilon*4,nStep=self.nStep)
    if np.size(relstep)==1:
        relstep = [relstep,0]

    ## Find damped mode
    for i, DQ_mCoh in enumerate(wmode__DQ):

        ## Calc DQ_mLD without taylorAlpha
        taylorAlpha= taylorAlphas[i]
        DQ_mLD,DQ_mFree,DQ_mSD,cnt,taylorAlpha, relerr = calc_LandauDampedOneMode(self.integrator_epsilon,integrator1,wmodeQ0,DQ_mCoh,
                                     relstep,tol=tol,flag_FindAlpha=flag_FindAlpha,flag_UpdateAlpha=flag_UpdateAlpha,taylorAlphaOld=taylorAlpha,debug=debug)
        if relerr>tol:
            print('WARNING in calc_LandauDampedAllModes: Consider cancelling due to relerr=%.1e>%.1e=tol\n  This is not done automatically.'%(relerr, tol))

        ## Set DQ_mLD
        if not(flag_UpdateReQ or np.abs(wmodeLDDQ[i])==0):
            DQ_mLD = wmodeLDDQ[i].real + 1j*DQ_mLD.imag

        ## Debugging
        if debug>0:
            print('calc_LandauDampedAllModes: Mode: %.2e %s%.2ei -> %.2e %s%.3ei (relerr(%d iterations)=%.1e , taylorAlpha=%.2f+%.2fi)'%(
                    DQ_mCoh.real,['+','-'][np.sign(DQ_mCoh.imag)<0],np.abs(DQ_mCoh.imag),DQ_mLD.real,
                    ['+','-'][np.sign(DQ_mLD.imag)<0],np.abs(DQ_mLD.imag),cnt,relerr,taylorAlpha.real,taylorAlpha.imag))

        wmodeLDDQ[i] = DQ_mLD
        wmodeSDDQ[i] = DQ_mSD
        taylorAlphas[i] = taylorAlpha

    if plane==0:
        self.M.wmodeLDDQx = wmodeLDDQ
        self.M.wmodeSDDQx = wmodeSDDQ
        self.M.taylorAlphax = taylorAlphas
    else:
        self.M.wmodeLDDQy = wmodeLDDQ
        self.M.wmodeSDDQy = wmodeSDDQ
        self.M.taylorAlphay = taylorAlphas
    return

def find_DiffusionSidebandWeight(sigma_x,maxOrder=50,tol=1e-3,debug=0):
    """
        !!! DEPRECATED
        !!! Currently believed to not be physically relevant
        Function to find the weight of the sideband
        Taken as average bessel function squared over the longitudinal gaussian distribution
    """
    orders = np.arange(maxOrder+1)
    factors = np.zeros(maxOrder+1)
    if sigma_x>0:
        xs = np.linspace(0,6,50)*sigma_x
        phi_x = xs/sigma_x**2 * np.exp(-xs**2/sigma_x**2*.5)

        for m in orders:
            ys   = sc.special.jv(m,xs)
            mean = sc.integrate.simps(ys**2*phi_x,x=xs,even='first')
            factors[m]=mean
            if factors[m]/factors[0]< tol:
                factors = factors[:m+1]
                orders = orders[:m+1]
                if debug>0:
                    print('find_DiffusionSidebandWeight: Need no more than %d sidebands: '%(m+1),factors)
                break
    else:
        factors = np.array([1])
        orders  = np.array([0])
    return factors,orders

#####################################################
## Diffusion with detuning and feedback (iCoeff==2)##

def LL(g2,dmu):
    numerator = (g2**2 + (1-g2)*(dmu)**2)
    LL = (1-g2)**2*(dmu)**2  / numerator
    LL[numerator==0] = 1
    return LL

def dLLdm(g2,dmu):
    return (1-g2)**2*g2**2*2*(dmu)            / (g2**2 + (1-g2)*(dmu)**2)**2


###########################
### Coefficients(Jx,Jy) ###
###########################

## Diffusion coefficient
def DiffCoeff1(D_ibs,DQ_inco):
    """
    Diffusion due to intra-beam scattering (normalized to the coordinate)
    """
    err=0
    return D_ibs * np.ones_like(DQ_inco) , err

def DiffCoeff2(D_ibs,D_k0,D_k1,DQ_inco,dQAvg,g):
    """
    Diffusion due to tune dependent damping (normalized to the coordinate)
    """
    err=0
    g2 = g/2
    dmu = 2*np.pi*(DQ_inco-dQAvg)
    return (D_ibs + D_k0 * LL(g2,dmu) + D_k1) , err

def DiffCoeff3b(D_ibs,D_k0,D_k1,Q0,DQ_inco,
                wmode__DQ,wmodeMom0,wmodeMom1,
                wmodeLDDQ,Qs=0):
    """
    DEPRECATED (see new below)
    Diffusion due to USHO-noise excited wakefields (normalized to the coordinate)
    """
    DD = D_ibs * np.ones_like(DQ_inco)
    err=0
    for i, DQ_mCoh in enumerate(wmode__DQ):
        # if i!=0: continue
        DQ_mLD = wmodeLDDQ[i]
        if DQ_mLD.imag>0:
            print('WARNING in DiffCoeff3: Instability - stop! growthrate=%.1e'%DQ_mLD.imag)
            DD *=0
            err =1
            break

        eta0   = wmodeMom0[i]
        eta1   = wmodeMom1[i]
        newQ2  =  (Q0+DQ_mLD.real)**2 - (DQ_mLD.imag)**2
        newQIR =  (Q0+DQ_mLD.real)*DQ_mLD.imag
        absDQ_mCoh2 = np.abs(DQ_mCoh)**2

        correction = (Q0*(Q0+DQ_mCoh.real) + absDQ_mCoh2/4)/(Q0+DQ_mLD.real)**2

        B = 1/(1 + (newQ2 - (Q0+DQ_inco)**2)**2/(4*(newQIR)**2))
        DD += (D_k0*eta0**2 + D_k1*eta1**2) * absDQ_mCoh2/(DQ_mLD.imag)**2 * B * correction


        if np.abs(correction-1)>1e-3:
            print('DiffCoeff3b: Correction of mode %i (DQ_coh=%.2e+%.2ej): 1+(%.1e)'%(i,DQ_mCoh.real,DQ_mCoh.imag,correction-1))
    return DD ,err

def DiffCoeff3(D_ibs,D_k0,D_k1,Q0,DQ_inco,
                wmode__DQ,wmodeMom0,wmodeMom1,
                wmodeLDDQ,Qs=0,flag_IncludeDirectNoise=True):
    """
    Diffusion due to USHO-noise excited wakefields (normalized to the coordinate)
    """
    DD = D_ibs * np.ones_like(DQ_inco)
    err=0
    for i, DQ_mCoh in enumerate(wmode__DQ):
        # if i!=0: continue
        DQ_mLD = wmodeLDDQ[i]
        if DQ_mLD.imag>0:
            print('WARNING in DiffCoeff3: Instability - stop! growthrate=%.1e'%DQ_mLD.imag)
            DD *=0
            err =1
            break

        eta0   = wmodeMom0[i]
        eta1   = wmodeMom1[i]

        if flag_IncludeDirectNoise:
            DQ = DQ_mLD - DQ_inco - DQ_mCoh
        else:
            DQ = - DQ_mCoh
        absDQ2 = DQ.real**2 +DQ.imag**2

        B_denominator = ( (DQ_mLD.real-DQ_inco)**2 + DQ_mLD.imag**2 )

        DD += (D_k0*eta0**2 + D_k1*eta1**2) * absDQ2 / B_denominator

    return DD ,err

def DiffCoeff5( D_ibs,D_k0,D_k1,DQ_inco,
                wmode__DQ,wmodeMom0,wmodeMom1,
                DQ_free,DQ_SD,interpOrderSD=1,Qs=0,flag_IncludeDirectNoise=True):
    """
    Diffusion due to DI-noise excited wakefields (normalized to the coordinate)
    Input parameters:
        > DQ_inco:   Real tune shifts ΔQ(J_x,J_y) where we want the diffusion coefficient
        > DQ_free:   List of real tune shifts
        > DQ_SD:     -1/DispersionIntegral(DQ_free). Neglecting some constant.
    Created parameters:
        > DI_inco:   Value of dispersion integral for input tune DQ_inco
    """
    DD = D_ibs * np.ones_like(DQ_inco)
    err=0

    ## Add diffusion from different modes
    ## WARNING: This can be a bottleneck for full LHC wake - uses ~1 second per 100 modes with 1000x1000 grid
    for i, DQ_mCoh  in enumerate(wmode__DQ):
        if i==0:
            DI = -1/DQ_SD
            interp_DI = sc.interpolate.interp1d(DQ_free,DI,
                            kind=['nearest','linear','quadratic','cubic'][interpOrderSD],
                            bounds_error=False,fill_value=(DI[0],DI[-1]))
            DI_inco = interp_DI(DQ_inco)

        eta0   = wmodeMom0[i]
        eta1   = wmodeMom1[i]

        if flag_IncludeDirectNoise:
            temp =           1      / (1 + DQ_mCoh * DI_inco)
        else:
            temp = -DQ_mCoh * DI_inco/ (1 + DQ_mCoh * DI_inco)
        DD += (D_k0*eta0**2 + D_k1*eta1**2) * (temp.real**2 + temp.imag**2)

    return DD,err



def DiffCoeffDQ(Mach,DQ_inco,plane,iCoeff,flag_IncludeDirectNoise=True):
    if iCoeff ==1:
        if plane==0:
            DD,err = DiffCoeff1(Mach.N.D_ibsx,DQ_inco)
        elif plane==1:
            DD,err = DiffCoeff1(Mach.N.D_ibsy,DQ_inco)
    elif iCoeff==2:
        if plane==0:
            DD,err = DiffCoeff2(Mach.N.D_ibsx, Mach.N.D_k0x, Mach.N.D_k1x,
                                DQ_inco, Mach.dQxAvg, Mach.gx)
        elif plane==1:
            DD,err = DiffCoeff2(Mach.N.D_ibsy, Mach.N.D_k0y, Mach.N.D_k1y,
                                DQ_inco, Mach.dQyAvg, Mach.gy)
    elif iCoeff==3:
        if plane==0:
            DD,err = DiffCoeff3(Mach.N.D_ibsx, Mach.N.D_k0x,Mach.N.D_k1x,
                                Mach.Q.Q0x, DQ_inco,
                                Mach.wmode__DQx,Mach.wmodeMom0x,Mach.wmodeMom1x,Mach.wmodeLDDQx,Qs=Mach.Qs,
                                flag_IncludeDirectNoise=flag_IncludeDirectNoise)
        elif plane==1:
            DD,err = DiffCoeff3(Mach.N.D_ibsy, Mach.N.D_k0y,Mach.N.D_k1y,
                                Mach.Q.Q0y, DQ_inco,
                                Mach.wmode__DQy,Mach.wmodeMom0y,Mach.wmodeMom1y,Mach.wmodeLDDQy,Qs=Mach.Qs,
                                flag_IncludeDirectNoise=flag_IncludeDirectNoise)
    elif iCoeff==4:
        ans1 = DiffCoeffDQ(Mach,DQ_inco,plane,1)  ## D_ibs
        ans2 = DiffCoeffDQ(Mach,DQ_inco,plane,2)  ## D_ibs + D_k0*LL   + D_k1
        ans3 = DiffCoeffDQ(Mach,DQ_inco,plane,3,flag_IncludeDirectNoise=flag_IncludeDirectNoise)  ## D_ibs + D_k0*USHO + D_k1*USHO
        DD  = ans2[0]+ans3[0]-ans1[0]
        err = ans2[1]+ans3[1]+ans1[1]

    elif iCoeff==5:
        if plane==0:
            DD,err = DiffCoeff5(Mach.N.D_ibsx, Mach.N.D_k0x,Mach.N.D_k1x,
                                DQ_inco, Mach.wmode__DQx,Mach.wmodeMom0x,Mach.wmodeMom1x,
                                Mach.Q_freex-Mach.Q.Q0x,Mach.DQ_SDx,Qs=Mach.Qs,
                                flag_IncludeDirectNoise=flag_IncludeDirectNoise)
        elif plane==1:
            DD,err = DiffCoeff5(Mach.N.D_ibsy, Mach.N.D_k0y,Mach.N.D_k1y,
                                DQ_inco, Mach.wmode__DQy,Mach.wmodeMom0y,Mach.wmodeMom1y,
                                Mach.Q_freey-Mach.Q.Q0y,Mach.DQ_SDy,Qs=Mach.Qs,
                                flag_IncludeDirectNoise=flag_IncludeDirectNoise)

    else:
        print('ERROR in DiffCoeffDQ: iCoeff in [1,2,3,4,5] only')
        DD=None
        err = 1
    return DD,err

def DiffCoeffJxJy(Mach,JX,JY,plane,iCoeff,flag_IncludeDirectNoise=True):
    if plane == 0:
        DQ_inco = Mach.Q.dQx(JX,JY)
    else:
        DQ_inco = Mach.Q.dQy(JX,JY)
    return DiffCoeffDQ(Mach,DQ_inco,plane,iCoeff,flag_IncludeDirectNoise=flag_IncludeDirectNoise)

def DiffCoeffGrid(Mach,Grid,iCoeff,flag_IncludeDirectNoise=True):
    # Noise: Mach.N
    # Detuning: Mach.Q
    DDx,errx = DiffCoeffJxJy(Mach,Grid.Jxbx2D,Grid.Jybx2D,plane=0,iCoeff=iCoeff,flag_IncludeDirectNoise=flag_IncludeDirectNoise)
    DDy,erry = DiffCoeffJxJy(Mach,Grid.Jxby2D,Grid.Jyby2D,plane=1,iCoeff=iCoeff,flag_IncludeDirectNoise=flag_IncludeDirectNoise)

    # Grid.X is the main coordinate (r, J)
    return Grid.X*DDx, Grid.Y*DDy, errx+erry



#######################
## Drift coefficient ##
## NO LONGER UPDATED ##
def DriftCoeff2(Jb,D_k0,damperAlpha,dQ,dQAvg,dQdJ,g):
    err=0
    g2 = g/2
    dmu = 2*np.pi*(dQ-dQAvg)
    return -Jb*((1-damperAlpha)*D_k0 * dLLdm(g2,dmu) * pi2*dQdJ) , err

def DriftCoeffJxJy(Mach,JP,JX,JY,plane,iCoeff):
    err=0
    if iCoeff in [1,3,4,5,6]:
        U = np.zeros_like(JX)
    elif iCoeff==2:
        if plane==0:
            U,err = DriftCoeff2(JP,Mach.N.D_k0x,Mach.damperAlpha,
                            Mach.Q.dQx(JX,JY),Mach.dQxAvg,Mach.Q.dQxdJx(JX,JY),Mach.gx)
        elif plane==1:
            U,err = DriftCoeff2(JP,Mach.N.D_k0y,Mach.damperAlpha,
                            Mach.Q.dQy(JX,JY),Mach.dQxAvg,Mach.Q.dQydJy(JX,JY),Mach.gy)
    else:
        print('ERROR in DriftCoeffJxJy: iCoeff in [1,2,3,4,5,6] only')
        U=None
        err = 1
    return U , err

def DriftCoeffGrid(Mach,Grid,iCoeff):
    # Noise: Mach.N
    # Detuning: Mach.Q
    Ux,errx = DriftCoeffJxJy(Mach,Grid.Jxbx2D,Grid.Jxbx2D,Grid.Jybx2D,plane=0,iCoeff=iCoeff)
    Uy,erry = DriftCoeffJxJy(Mach,Grid.Jyby2D,Grid.Jxby2D,Grid.Jyby2D,plane=1,iCoeff=iCoeff)
    return Ux,Uy,errx+erry
