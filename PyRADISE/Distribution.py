import numpy as np
from scipy import interpolate

class MyDistribution1D(object):
    def __init__(self,Jb=0,Jc=0,Psic=0,dPsi=0,flag_Radial=0,interpOrderPsi=1):
        self._Jb=Jb.tolist()
        self._Jc=Jc.tolist()
        self._Psic=Psic.tolist() #+[0]
        self._dPsi=dPsi.tolist() #+[0]

        kind = ["nearest",'slinear','quadratic','cubic'][interpOrderPsi]
        self.interp_psi     = interpolate.interp1d(self._Jc,      self._Psic,
                                                    kind=kind,bounds_error=False,fill_value=(self._Psic[0][0],self._Psic[0][-1]))#"extrapolate")
        self.interp_dPsidJx = interpolate.interp1d(self._Jb[1:-1],self._dPsi,
                                                    kind=kind,bounds_error=False,fill_value=(self._dPsi[0][0],self._dPsi[0][-1]))#"extrapolate")

    def getValue(self, jx, jy):
        return (self.interp_psi(jx)*np.exp(-jy))[0]

    def getDJx(self, jx, jy):
        return (self.interp_dPsidJx(jx)*np.exp(-jy))[0]

    def getDJy(self, jx, jy):
        return (-self.getValue(jx, jy))[0]



class MyDistribution1Dy(MyDistribution1D):
    def __init__(self,Jb=0,Jc=0,Psic=0,dPsi=0,flag_Radial=0,interpOrderPsi=1):
        MyDistribution1D.__init__(self,Jb,Jc,Psic,dPsi,flag_Radial,interpOrderPsi)
    def getDJx(self,jy,jx):
        return super().getDJy(jx,jy)
    def getDJy(self,jy,jx):
        return super().getDJx(jx,jy)


class MyDistribution2D(object):
    def __init__(self,Jxbx=0,Jxc=0,Jyby=0,Jyc=0,Psic=0,dPsidJx=0,dPsidJy=0,flag_Radial=0,interpOrderPsi=0):
        self._Jxbx= Jxbx.copy()
        self._Jyby= Jyby.copy()
        self._Jxc=Jxc.copy()
        self._Jyc=Jyc.copy()

        self._Psic=Psic.copy()
        self._dPsidJx=dPsidJx.copy()
        self._dPsidJy=dPsidJy.copy()
        self._interpOrderPsi= interpOrderPsi

        if interpOrderPsi==0:
            temp_interp_psi = interpolate.RegularGridInterpolator((self._Jxc,self._Jyc),self._Psic.T,
                                   method='nearest',bounds_error=False,fill_value=None)
            temp_interp_dPsidJx = interpolate.RegularGridInterpolator((self._Jxbx[1:-1],self._Jyc),self._dPsidJx.T,
                                   method='nearest',bounds_error=False,fill_value=None)
            temp_interp_dPsidJy = interpolate.RegularGridInterpolator((self._Jxc,self._Jyby[1:-1]),self._dPsidJy.T,
                                   method='nearest',bounds_error=False,fill_value=None)
            self.interp_psi     = temp_interp_psi
            self.interp_dPsidJx = temp_interp_dPsidJx
            self.interp_dPsidJy = temp_interp_dPsidJy
        else :
            ## Notice shift of input (Jy,Jx) when using RectBivariateSpline
            J0 = 0
            temp_interp_psi     = interpolate.RectBivariateSpline(self._Jyc,self._Jxc,self._Psic,
                                                                 kx=interpOrderPsi,ky=interpOrderPsi,bbox=[J0,Jyby[-1],J0,Jxbx[-1]])
            J0 = None  ## constant value outside box for the derivative.
            temp_interp_dPsidJx = interpolate.RectBivariateSpline(self._Jyc,self._Jxbx[1:-1],self._dPsidJx,
                                                                 kx=interpOrderPsi,ky=interpOrderPsi,bbox=[J0,self._Jyc[-1],J0,Jxbx[-2]])
            temp_interp_dPsidJy = interpolate.RectBivariateSpline(self._Jyby[1:-1],self._Jxc,self._dPsidJy,
                                                                 kx=interpOrderPsi,ky=interpOrderPsi,bbox=[J0,Jyby[-2],J0,self._Jxc[-1]])

            self.interp_psi     = temp_interp_psi
            self.interp_dPsidJx = temp_interp_dPsidJx
            self.interp_dPsidJy = temp_interp_dPsidJy


    def getValue(self, jx, jy=0):
        if self._interpOrderPsi==0:
            return self.interp_psi((jx,jy))
        else:
            return self.interp_psi(jy,jx,grid=False)

    def getDJx(self, jx, jy=0):
        if self._interpOrderPsi==0:
            return self.interp_dPsidJx((jx,jy))
        else:
            return self.interp_dPsidJx(jy,jx,grid=False)

    def getDJy(self, jx, jy=0):
        if self._interpOrderPsi==0:
            return self.interp_dPsidJy((jx,jy))
        else:
            return self.interp_dPsidJy(jy,jx,grid=False)

class MyDistribution2Dy(MyDistribution2D):
    def __init__(self,Jxbx=0,Jxc=0,Jyby=0,Jyc=0,Psic=0,dPsidJx=0,dPsidJy=0,flag_Radial=0,interpOrderPsi=0):
        MyDistribution2D.__init__(self,Jyby,Jyc,Jxbx,Jxc,Psic.T,dPsidJy.T,dPsidJx.T,flag_Radial,interpOrderPsi)
        return
    """
    Only implemented because original PySSD only calculated stability diagram in the horizontal plane.
    Hence, this simply transposes the original distribution, exchanging X and Y.
    """
