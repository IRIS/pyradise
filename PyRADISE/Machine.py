import copy
import numpy as np
import PyRADISE

class MachineClass(object):
    def __init__(self,detuning,noise,gx=0,gy=0,f_rev = 11245,
                 wmodeQ0x=[],wmode__DQx=[],wmodeMom0x=[],wmodeMom1x=[],
                 wmodeQ0y=[],wmode__DQy=[],wmodeMom0y=[],wmodeMom1y=[],
                 Qpx=0,Qpy=0,Qs=0.002,sigma_dpp=1e-4,
                 dQxAvg=0,dQyAvg=0,damperAlpha=1):
        self.Q = detuning
        self.Qy= copy.deepcopy(detuning)
        self.Q.call_plane=0 ; self.Qy.call_plane=1
        self.N = noise

        self.gx = gx
        self.gy = gy
        self.f_rev = f_rev

        ## Parameters for iCoeff==3:
        self.dQxAvg = dQxAvg
        self.dQyAvg = dQyAvg
        self.damperAlpha  = damperAlpha

        ## Longitudinal dynamics - not necessarily used to anything at the moment... (but can be related to the wake modes)
        self.Qpx= Qpx
        self.Qpy= Qpy
        self.Qs = Qs
        self.sigma_dpp = sigma_dpp

        ## Horizontal Wake in np.arrays
        self.flag_Wmode= np.size(wmode__DQx)+np.size(wmode__DQy)>0
        self.nWmodex = nWmodex = np.min([np.size(wmode__DQx),np.size(wmodeQ0x),np.size(wmodeMom0x),np.size(wmodeMom1x)])
        self.wmodeQ0x   = np.array([wmodeQ0x  ]) if np.shape(wmodeQ0x  )==() else np.array(wmodeQ0x[:nWmodex])      # Currently not used to anything
        self.wmode__DQx = np.array([wmode__DQx]) if np.shape(wmode__DQx)==() else np.array(wmode__DQx[:nWmodex])
        self.wmodeLDDQx = np.zeros_like(self.wmode__DQx)
        self.wmodeSDDQx = np.zeros_like(self.wmode__DQx)
        self.wmodeMom0x = np.array([wmodeMom0x]) if np.shape(wmodeMom0x)==() else np.array(wmodeMom0x[:nWmodex])
        self.wmodeMom1x = np.array([wmodeMom1x]) if np.shape(wmodeMom1x)==() else np.array(wmodeMom1x[:nWmodex])
        self.taylorAlphax = np.zeros(nWmodex,dtype=np.complex)

        ## Vertical Wake in np.arrays
        self.nWmodey = nWmodey = np.min([np.size(wmode__DQy),np.size(wmodeQ0y),np.size(wmodeMom0y),np.size(wmodeMom1y)])
        self.wmodeQ0y   = np.array([wmodeQ0y  ]) if np.shape(wmodeQ0y  )==() else np.array(wmodeQ0y[:nWmodey])      # Currently not used to anything
        self.wmode__DQy = np.array([wmode__DQy]) if np.shape(wmode__DQy)==() else np.array(wmode__DQy[:nWmodey])
        self.wmodeLDDQy = np.zeros_like(self.wmode__DQy)
        self.wmodeSDDQy = np.zeros_like(self.wmode__DQy)
        self.wmodeMom0y = np.array([wmodeMom0y]) if np.shape(wmodeMom0y)==() else np.array(wmodeMom0y[:nWmodey])
        self.wmodeMom1y = np.array([wmodeMom1y]) if np.shape(wmodeMom1y)==() else np.array(wmodeMom1y[:nWmodey])
        self.taylorAlphay = np.zeros(nWmodey,dtype=np.complex)

        ## Stablity diagram for interpolation
        self.Q_freex = np.zeros(1)
        self.Q_freey = np.zeros(1)
        self.DQ_SDx = np.zeros(1,dtype=np.complex64)
        self.DQ_SDy = np.zeros(1,dtype=np.complex64)
