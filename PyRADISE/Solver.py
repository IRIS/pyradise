import numpy as np
from scipy import integrate,sparse
import time
import sys
import gc

import matplotlib.pyplot as plt

import PyRADISE

class SolverClass(  PyRADISE.Plotting.PlottingClass,
                    PyRADISE.PostProcessing.PostProcessingClass,
                    PyRADISE.StabilityDiagram.StabilityDiagramClass) :
    def __init__(self,grid,machine,iCoeff,tsODE,ntPSImax=20,flag_ICPoint=0,
                    solve_method='BDF',interpOrderPsi=1,
                    flag_FindAlpha=1, flag_UpdateReQ=1,flag_UpdateAlpha=1,
                    flag_UpdateCoeff=1,flag_UpdateCoeffAllTheWay=1,flag_AdjustTmax=0,
                    flag_SDRemoveLoops=0,relInstabilityThreshold = 0,flag_IncludeDirectNoise=1,
                    debug=0,maxCPU=1,flag_Print=True):

        # Solver
        self.iCoeff=iCoeff
        self.solve_method=solve_method
        self.interpOrderPsi=interpOrderPsi
        self.flag_ICPoint = flag_ICPoint
        self.flag_AdjustTmax=flag_AdjustTmax
        self.flag_UpdateCoeff=flag_UpdateCoeff
        self.flag_UpdateCoeffAllTheWay=flag_UpdateCoeffAllTheWay
        self.flag_SDRemoveLoops = flag_SDRemoveLoops
        self.relInstabilityThreshold = relInstabilityThreshold
        self.flag_IncludeDirectNoise = flag_IncludeDirectNoise

        self.flag_storeSDWhileSolving = False
        if flag_UpdateCoeff and self.iCoeff in [5,6]:
            self.flag_storeSDWhileSolving = True

        self.setTimeODE(tsODE)
        self.ntPSImax = ntPSImax
        self.setTimePSI()

        self.debug  = debug
        self.maxCPU = maxCPU


        # ...
        self.G = grid
        self.ND = self.G.ND

        self.M = machine
        if self.M.nWmodex>0:
            self.indLeastStablex= np.zeros(self.ntODE,dtype=np.int32) -1
            self.growthRatex = np.zeros(self.ntODE)
            self.stabilityMarginx  = np.zeros(self.ntODE)
            self.stabilityMargins0x= np.ones(self.M.nWmodex)
        if self.M.nWmodey>0:
            self.indLeastStabley= np.zeros(self.ntODE,dtype=np.int32) -1
            self.growthRatey = np.zeros(self.ntODE)
            self.stabilityMarginy = np.zeros(self.ntODE)
            self.stabilityMargins0y= np.ones(self.M.nWmodey)
        self.latNumx = None
        self.latNumy = None


        ## Default initialization of hirearchial classes
        self.postProc_init(flag_Print=False)
        self.SD_init(flag_Print=False)
        self.plot_init(flag_Print=False)


        ## For finding damped mode with Taylor approach
        self.relstep = [0.5,0.05]
        self.tol = 1e-4
        self.flag_FindAlpha = [flag_FindAlpha, [flag_FindAlpha,flag_FindAlpha]][np.size(flag_FindAlpha)==1]
        self.flag_UpdateReQ = flag_UpdateReQ
        self.flag_UpdateAlpha=flag_UpdateAlpha


        ## Set initial condition
        self._IC()

        if flag_Print:
            self._solver_print()

    def _solver_print(self,):
        print('SolverClass is initialized with:',
                '\n  iCoeff=%d'%self.iCoeff,
                '\n  solve_method=%s'%self.solve_method,
                '\n  interpOrderPsi=%d'%self.interpOrderPsi,
                '\n  flag_storeSDWhileSolving=%d'%self.flag_storeSDWhileSolving,
                '\n  flag_AdjustTmax=%d'%self.flag_AdjustTmax,
                '\n  flag_UpdateCoeff=%d'%self.flag_UpdateCoeff,
                '\n  flag_UpdateCoeffAllTheWay=%d'%self.flag_UpdateCoeffAllTheWay,
                '\n  relInstabilityThreshold=%.2f'%self.relInstabilityThreshold,
                '\n  flag_IncludeDirectNoise=%d'%self.flag_IncludeDirectNoise,
                '\n  flag_ICPoint=%d'%self.flag_ICPoint,
                '\n  debug=%d'%self.debug,
                )

    ############################
    ## Initializing functions ##
    def solver_init(self,**kwargs):
        self.solver_calcModesAndSD(self.psi0,**kwargs)
        if self.flag_storeSDWhileSolving or self.debug > 1:
            self.solver_storeSD(0)
            if self.debug > 1:
                self.flag_SDAllx = (self.M.nWmodex>0 and self.flag_storeSDWhileSolving)
                self.flag_SDAlly = (self.M.nWmodey>0 and self.flag_storeSDWhileSolving)
        time0=time.time()
        flag_Unstable = self.solver_checkStability(iTime=0,**kwargs)

        ## Perhaps need to calculate stability diagram
        if self.widthRatioSD>0 and self.widthRatioSD<1 and self.iCoeff in [5,6]:
            if     ((self.M.nWmodex>0 and (self.indLeastStablex[0] != np.argmax(self.M.wmode__DQx.imag)))
                or  (self.M.nWmodey>0 and (self.indLeastStabley[0] != np.argmax(self.M.wmode__DQy.imag)))):
                print('solver_init: Recalculate the stability diagram because center_tune has been adjusted.',
                        self.indLeastStablex[0],np.argmax(self.M.wmode__DQx.imag))
                self.solver_calcModesAndSD(self.psi0,**kwargs)
                flag_Unstable = self.solver_checkStability(iTime=0,**kwargs)

        if flag_Unstable:
            print('solver_init: Initial conditions are unstable. Abort calculation.')
            if self.debug > 1:
                self.SD_plotSD(plane=0)
                plt.show()
                print('DEBUG in solver_init: Press enter to continue running.')
                input()
            sys.exit()

        self.solver_calcCoeff(**kwargs)
        #Analytical Latency
        self.solver_analyticalLatency()
        return

    def setTime(self,tsODE,**kwargs):
        self.setTimeODE(tsODE,**kwargs)
        self.setTimeSD(**kwargs)
        if np.size(self.indexSD)>0:
            self.setTimePSI(indexPSI=self.indexSD,**kwargs)
        else:
            self.setTimePSI(**kwargs)
        return

    def setTimeODE(self,tsODE,**kwargs):
        self.tsODE = tsODE                      # Times used to solve for PSI(T)
        self.tmax = np.max(self.tsODE)
        self.ntODE = np.size(self.tsODE)

        self.indexGRx = np.arange(self.ntODE)
        self.indexGRy = np.arange(self.ntODE)
        self.indexSMx = np.arange(self.ntODE)
        self.indexSMy = np.arange(self.ntODE)

        return

    def setTimePSI(self,**kwargs):
        if 'indexPSI' in kwargs:
            self.indexPSI = kwargs.get('indexPSI',[0])
        else:
            if self.ntPSImax>1:
                dtPSI = int(np.ceil((self.ntODE-1)/(self.ntPSImax-1)))       # Step between calculated PSI to store
                self.indexPSI = np.arange(0,self.ntODE+dtPSI-1,dtPSI)        # What indexes of tsODE to store PSI for
                self.indexPSI[-1]=self.ntODE-1
            elif self.ntPSImax ==1:
                self.indexPSI=[self.ntODE-1]
            else:
                self.indexPSI=[]
        self.tsPSI = self.tsODE[self.indexPSI]       # Times at which to store PSI(T)
        self.ntPSI = np.size(self.tsPSI)
        return

    def setTimeSD(self,**kwargs):
        if 'indexSD' in kwargs:
            self.indexSD = kwargs.get('indexSD',[0])
        else:
            if self.ntSDmax>1:
                dtSD = int(np.ceil((self.ntODE-1)/(self.ntSDmax-1)))  # Step between calculated PSI to calculate SD for
                self.indexSD = np.arange(0,self.ntODE+dtSD-1,dtSD)    # What indexes of tsODE to calculate SD for
                self.indexSD[-1]=self.ntODE-1
            elif self.ntSDmax ==1:
                self.indexSD=[-1]
            else:
                self.indexSD=[]

        self.tsSD = self.tsODE[self.indexSD]
        self.ntSD = np.size(self.tsSD)
        return

    #############################
    ## Adjust the distribution ##
    def getPsi1D(self,psi):
        psi1D = psi.reshape((self.G.nrow,self.G.ncol))
        dPsi1DdJx   = np.diff(psi1D) / self.G.centersepx
        return psi1D , dPsi1DdJx

    def getPsi2D(self,psi):
        psi2D = psi.reshape((self.G.nrow,self.G.ncol))
        dPsi2DdJx = np.diff(psi2D,axis=1) / self.G.centersepx
        dPsi2DdJy = None
        if self.ND >1:
            dPsi2DdJy = np.diff(psi2D,axis=0) / self.G.centersepy[:,np.newaxis]
        return psi2D,dPsi2DdJx,dPsi2DdJy

    def calcInterpDist(self,psi,plane=0,flag_BC_gradient_0=False,**kwargs):
        interpOrderPsi = kwargs.pop('interpOrderPsi',self.interpOrderPsi)

        # nrow = self.G.nrow
        # ncol = self.G.ncol
        # centersepx = np.diff(self.G.Jxc)
        # centersepy = np.diff(self.G.Jyc)

        if self.ND==1:
            if plane ==0: Func = PyRADISE.Distribution.MyDistribution1D
            else:         Func = PyRADISE.Distribution.MyDistribution1Dy
            psi1D , dPsi1DdJx = self.getPsi1D(psi)
            ## Enforce zero gradient at Jmax
            if flag_BC_gradient_0:
                dPsi1DdJx[:,-1]=0
            distribution = Func(self.G.Jxbx,self.G.Jxc, psi1D,dPsi1DdJx,
                                flag_Radial=self.G.flag_Radial,interpOrderPsi=interpOrderPsi)
        else:
            if plane ==0: Func = PyRADISE.Distribution.MyDistribution2D
            else:         Func = PyRADISE.Distribution.MyDistribution2Dy
            # psi2D = psi.reshape((nrow,ncol))
            # dPsi2DdJx = np.diff(psi2D,axis=1) / centersepx
            # dPsi2DdJy = np.diff(psi2D,axis=0) / centersepy[:,np.newaxis]
            psi2D,dPsi2DdJx,dPsi2DdJy = self.getPsi2D(psi)
            ## Enforce zero gradient at Jmax
            if flag_BC_gradient_0:
                dPsi2DdJx[:,-1] = 0  ; dPsi2DdJx[-1,:] = 0
                dPsi2DdJy[:,-1] = 0  ; dPsi2DdJy[-1,:] = 0
            distribution = Func(self.G.Jxbx,self.G.Jxc,self.G.Jyby,self.G.Jyc,
                                psi2D,dPsi2DdJx,dPsi2DdJy,
                                flag_Radial=self.G.flag_Radial,interpOrderPsi=interpOrderPsi)
        if plane==0:
            self.interpDistx = distribution
        else:
            self.interpDisty = distribution
        return distribution


    ##########################
    ## Physics Coefficients ##
    def solver_calcModes(self,psi,**kwargs):
        # Calculate distribution
        relstep = kwargs.pop('relstep',self.relstep)
        tol     = kwargs.pop('tol',self.tol)
        debug   = kwargs.pop('debug',self.debug)
        flag_FindAlpha=kwargs.pop('flag_FindAlpha',self.flag_FindAlpha)
        flag_UpdateReQ=kwargs.pop('flag_UpdateReQ',self.flag_UpdateReQ)
        flag_UpdateAlpha=kwargs.pop('flag_UpdateAlpha',self.flag_UpdateAlpha)
        for plane in [0,1][:self.ND]:
            self.calcInterpDist(psi,plane=plane,flag_BC_gradient_0=True)
            PyRADISE.Coefficients.calc_LandauDampedAllModes(self,plane=plane,relstep=relstep,tol=tol,flag_FindAlpha=flag_FindAlpha,
                                      flag_UpdateReQ=flag_UpdateReQ,debug=debug,flag_UpdateAlpha=flag_UpdateAlpha)
        return

    def solver_calcSD(self, psi, **kwargs):
        if self.M.nWmodex>0:
            self.SD_calcSinglePsi(psi,planes=[0])

        if self.M.nWmodey>0:
            self.SD_calcSinglePsi(psi,planes=[1])
        return

    def solver_storeSD(self,iTime):
        """
        Store the stability diagram while solving the PDE!
        """
        if np.size(self.M.Q_freex)==self.nQ:
            self.Q_freeAllx[iTime,:]=self.M.Q_freex.copy()
            self.DQ_SDAllx[iTime,:] =self.M.DQ_SDx.copy()
            self.flag_SDAllx = True
        if np.size(self.M.Q_freey)==self.nQ:
            self.Q_freeAlly[iTime,:]=self.M.Q_freey.copy()
            self.DQ_SDAlly[iTime,:] =self.M.DQ_SDy.copy()
            self.flag_SDAlly = True
        return

    def solver_calcModesAndSD(self,psi,flag_calcSD=False,**kwargs):
        if self.M.flag_Wmode:
            if self.iCoeff in [3,4]:
                ## 1: Calculate Landau damped modes
                self.solver_calcModes(psi,**kwargs)

            if self.iCoeff in [5,6] or flag_calcSD :
                ## 1: Calculate stability diagram
                self.solver_calcSD(psi,**kwargs)
        return

    def solver_checkStability(self,iTime=-1,**kwargs):
        self.flag_Unstablex = False
        self.flag_Unstabley = False

        if self.M.flag_Wmode:
            if self.iCoeff in [3,4]:
                if self.M.nWmodex>0:
                    indLeastStable = np.argmax(self.M.wmodeLDDQx.imag)
                    growthRatex     = self.M.wmodeLDDQx[indLeastStable].imag
                    stabilityMargin = self.M.wmodeSDDQx[indLeastStable].imag - self.M.wmode__DQx[indLeastStable].imag
                    # stabilityMargin = PyRADISE.L2D2_functions.getDQSD(0,self.M.wmodeLDDQx[indLeastStable],self.M.taylorAlphax[indLeastStable])[1].imag
                    if iTime>=0:
                        self.wmodeTestx = self.M.wmode__DQx[indLeastStable]
                        self.indLeastStablex[iTime] = indLeastStable
                        self.growthRatex[iTime]     = growthRatex
                        self.stabilityMarginx[iTime]= stabilityMargin
                    ## Store initial stability margin for all modes
                    if iTime==0:
                        for iMode in range(self.M.nWmodex):
                            if self.M.wmode__DQx[iMode].imag<0:
                                self.stabilityMargins0x[iMode] = 1e9
                            else:
                                self.stabilityMargins0x[iMode] = self.M.wmodeSDDQx[iMode].imag - self.M.wmode__DQx[iMode].imag
                                # self.stabilityMargins0x[iMode] = PyRADISE.L2D2_functions.getDQSD(0,self.M.wmodeLDDQx[iMode],self.M.taylorAlphax[iMode])[1].imag
                    ## Check stability
                    if stabilityMargin/self.stabilityMargins0x[indLeastStable] < self.relInstabilityThreshold:
                        self.flag_Unstablex = True

                if self.M.nWmodey>0:
                    indLeastStable = np.argmax(self.M.wmodeLDDQy.imag)
                    growthRatey = self.M.wmodeLDDQy[indLeastStable].imag
                    stabilityMargin = self.M.wmodeSDDQy[indLeastStable].imag - self.M.wmode__DQy[indLeastStable].imag
                    # stabilityMargin = PyRADISE.L2D2_functions.getDQSD(0,self.M.wmodeLDDQy[indLeastStable],self.M.taylorAlphay[indLeastStable])[1].imag
                    if iTime>=0:
                        self.wmodeTestx = self.M.wmode__DQy[indLeastStable]
                        self.indLeastStabley[iTime]= indLeastStable
                        self.growthRatey[iTime] = growthRatey
                        self.stabilityMarginy[iTime] = stabilityMargin
                    ## Store initial stability margin for all modes
                    if iTime==0:
                        for iMode in range(self.M.nWmodey):
                            if self.M.wmode__DQy[iMode].imag<0:
                                self.stabilityMargins0y[iMode] = 1e9
                            else:
                                self.stabilityMargins0y[iMode] = self.M.wmodeSDDQy[iMode].imag - self.M.wmode__DQy[iMode].imag
                                # self.stabilityMargins0y[iMode] = PyRADISE.L2D2_functions.getDQSD(0,self.M.wmodeLDDQy[iMode],self.M.taylorAlphay[iMode])[1].imag
                    ## Check stability
                    if stabilityMargin/self.stabilityMargins0y[indLeastStable] < self.relInstabilityThreshold:
                        self.flag_Unstabley = True

            elif self.iCoeff in [5,6]:
                if self.M.nWmodex>0:
                    stabilityMargins,stabilityMargin,indLeastStable,wmode = self.SD_calcStabilityMarginAllModesOneSD(0,self.M.DQ_SDx)
                    if iTime>=0:
                        self.wmodeTestx = wmode
                        self.indLeastStablex[iTime]= indLeastStable
                        self.growthRatex[iTime] = -stabilityMargin
                        self.stabilityMarginx[iTime] = stabilityMargin
                    ## Store initial stability margin for all modes
                    if iTime==0:
                        self.stabilityMargins0x = stabilityMargins
                    ## Check stability
                    if stabilityMargin/self.stabilityMargins0x[indLeastStable] < self.relInstabilityThreshold:
                        self.flag_Unstablex = True
                if self.M.nWmodey>0:
                    stabilityMargins,stabilityMargin,indLeastStable,wmode = self.SD_calcStabilityMarginAllModesOneSD(1,self.M.DQ_SDy)
                    if iTime>=0:
                        self.wmodeTesty = wmode
                        self.indLeastStabley[iTime]= indLeastStable
                        self.growthRatey[iTime] = -stabilityMargin
                        self.stabilityMarginy[iTime] = stabilityMargin
                    ## Store initial stability margin for all modes
                    if iTime==0:
                        self.stabilityMargins0y = stabilityMargins
                    ## Check stability
                    if stabilityMargin/self.stabilityMargins0y[indLeastStable] < self.relInstabilityThreshold:
                        self.flag_Unstabley = True
        return (self.flag_Unstablex or self.flag_Unstabley)

    def solver_calcCoeff(self,**kwargs):
        Diffx, Diffy ,errDiff  = PyRADISE.Coefficients.DiffCoeffGrid(self.M,self.G,self.iCoeff,flag_IncludeDirectNoise=self.flag_IncludeDirectNoise)
        Driftx,Drifty,errDrift = PyRADISE.Coefficients.DriftCoeffGrid(self.M,self.G,self.iCoeff)
        if errDiff+errDrift:
            print('ERROR in solver_calcCoeff: Cannot calculate diffusion or drift coefficients - possibly unstable.')
            return errDiff+errDrift

        self.DiffE  = np.reshape(Diffx[self.G.sliceE], (self.G.NcTot,1))
        self.DiffW  = np.reshape(Diffx[self.G.sliceW], (self.G.NcTot,1))
        self.DriftE = np.reshape(Driftx[self.G.sliceE], (self.G.NcTot,1))
        self.DriftW = np.reshape(Driftx[self.G.sliceW], (self.G.NcTot,1))

        if self.ND >1:
            self.DiffN  = np.reshape(Diffy[self.G.sliceN], (self.G.NcTot,1))
            self.DiffS  = np.reshape(Diffy[self.G.sliceS], (self.G.NcTot,1))
            self.DriftN = np.reshape(Drifty[self.G.sliceN], (self.G.NcTot,1))
            self.DriftS = np.reshape(Drifty[self.G.sliceS], (self.G.NcTot,1))

        self._setFV_M()
        self._setBC_V()

        ## Delete helper matrices to reduce memory consumption
        del(self.DiffE)
        del(self.DiffW)
        del(self.DriftE)
        del(self.DriftW)
        if self.ND >1 :
            del(self.DiffN)
            del(self.DiffS)
            del(self.DriftN)
            del(self.DriftS)

        return 0

    def _setFV_M(self):
        self.FV_M= ((self.G.MslpE.multiply(self.DiffE) - self.G.MslpW.multiply(self.DiffW))-
                    (self.G.MavgE.multiply(self.DriftE) - self.G.MavgW.multiply(self.DriftW)))
        if self.ND==2:
            self.FV_M += ((self.G.MslpN.multiply(self.DiffN)  - self.G.MslpS.multiply(self.DiffS))-
                          (self.G.MavgN.multiply(self.DriftN) - self.G.MavgS.multiply(self.DriftS)))
        return

    def _setBC_V_1D(self,iBC,K,h,boundary,Diff,Drift,rc):
        ## Homogeneous BC
        if K==0:
            return 0

        ## Create Diff and Drift vectors including matrices
        V_Diff = Diff/h**2
        V_Drift= Drift/(2*h)
        if self.G.flag_Radial:
            V_Diff /= (2*rc)
            V_Drift/= (rc)

        ## Return the values
        if   iBC==0:
            return K*boundary * (V_Diff - V_Drift)      #+vertical part
        elif iBC==1:
            return K*boundary * (V_Diff - V_Drift)*h    #+vertical part
        elif iBC>1:
            print("WARNING in _setBC_V_1D: Have not implemented nonhomogneeous BC for iBC>1 - use Homogeneous BC")
            return 0

    def _setBC_V(self):
        self.BC_V = 0
        #Boundary at xmax
        if self.ND==1:
            self.BC_V += self._setBC_V_1D(self.G.BC_xMax,self.G.BC_xVal, self.G.hx,
                                       self.G.boundary_E,self.DiffE[:,0],self.DriftE[:,0],self.G.rxc[-1])
        elif self.ND==2:
            self.BC_V += self._setBC_V_1D(self.G.BC_xMax,self.G.BC_xVal, self.G.hx,
                                       self.G.boundary_E,self.DiffE[:,0],self.DriftE[:,0],self.G.rxc[-1])
            self.BC_V += self._setBC_V_1D(self.G.BC_yMax,self.G.BC_yVal, self.G.hy,
                                       self.G.boundary_N,self.DiffN[:,0],self.DriftN[:,0],self.G.ryc[-1])

    ########################
    ## Initial conditions ##
    def _IC(self):
        self.sigx0 = sigx0 = 1

        self.G.Jxbx2D[self.G.sliceE]
        if self.flag_ICPoint: self.psi0 =    (1/sigx0**2*np.exp(-[self.G.Jxc1D,.5*self.G.rxc1D**2][self.G.flag_Radial]/(sigx0**2)))
        else:                 self.psi0 =    (np.exp(-self.G.Jxbx2D[self.G.sliceW]/sigx0**2)-np.exp(-self.G.Jxbx2D[self.G.sliceE]/sigx0**2))/np.diff(self.G.Jxbx2D,axis=1)
        if self.ND==2:
            self.sigy0 = sigy0 = 1
            if self.flag_ICPoint:   self.psi0 *=1/sigy0**2*np.exp(-[self.G.Jyc1D,.5*self.G.ryc1D**2][self.G.flag_Radial]/(sigy0**2))
            else:                   self.psi0 *= (np.exp(-self.G.Jyby2D[self.G.sliceS]/sigy0**2)-np.exp(-self.G.Jyby2D[self.G.sliceN]/sigy0**2))/np.diff(self.G.Jyby2D,axis=0)
        self.psi0 = self.psi0.flatten()
        return

    def setIC(self,psi0):
        self.psi0 = psi0
        return


    ########################
    ## Analytical Latency ##
    def solver_analyticalLatency(self,flag_RecalcModes=0,**kwargs):
        """
        Calculate analytical latency based on the USHO model (iCoeff in [3,4]).
        """
        self.latEstx  = np.nan
        self.latQuadx = np.nan
        self.latEsty  = np.nan
        self.latQuady = np.nan
        if  not(self.M.flag_Wmode and self.iCoeff in [3,4,5,6]) :
            return

        # if flag_RecalcModes :
        #     self.solver_calcModes(self.psi0,**kwargs)

        Trev = 1/self.M.f_rev

        ## Horizontal latency
        if self.M.nWmodex>0:
            if np.any(self.indLeastStablex>=0):
                ## If have found the least stable mode, use that instead.
                iMode = self.indLeastStablex[self.indLeastStablex>=0][-1]
            else:
                ## Else, use the one with the largest imaginary part
                iMode = np.argmax(self.M.wmode__DQx.imag)
            DQ_mCoh = self.M.wmode__DQx[iMode]
            DQ_mLD      = self.M.wmodeLDDQx[iMode]
            taylorAlpha = self.M.taylorAlphax[iMode]
            if flag_RecalcModes or abs(DQ_mLD)==0:
                Q0   = self.M.Q.Q0x
                detuning = self.M.Q
                distribution = self.calcInterpDist(self.psi0,plane=0,flag_BC_gradient_0=True)
                DQ_mLD,DQ_mFree,DQ_mSD,cnt,taylorAlpha, relerr = self.SD_calcGrowthRateOneModeOnePsi(Q0,DQ_mCoh,distribution,detuning,**kwargs)

            latEst,latQuad,L_L = PyRADISE.L2D2_functions.analyticalLatency(
                            DQ_mCoh = DQ_mCoh,DQ_mLD=DQ_mLD,taylorAlpha=taylorAlpha,
                            ax = self.M.Q.ax,bx = self.M.Q.bx,
                            eta0=self.M.wmodeMom0x[iMode],sigma_k0=self.M.N.sigma_k0x,
                            eta1=self.M.wmodeMom1x[iMode],sigma_k1=self.M.N.sigma_k1x,Trev = 1/self.M.f_rev)

            print("solver_analyticalLatency: Analytical horizontal Latency estimate = %.3es = %.3es * %.3f"%(latEst,L_L,latEst/L_L))
            self.latEstx = latEst
            print("solver_analyticalLatency: Analytical horizontal Latency quadintg = %.3es = %.3es * %.3f"%(latQuad,L_L,latQuad/L_L))
            self.latQuadx = latQuad
            print("                          (for mode nr %d: DQ= %.3e + %.3ej)"%(iMode,DQ_mCoh.real,DQ_mCoh.imag))

        ## Vertical latency
        if self.M.nWmodey>0:
            if np.any(self.indLeastStabley>=0):
                ## If have found the least stable mode, use that instead.
                iMode = self.indLeastStabley[self.indLeastStabley>=0][-1]
            else:
                ## Else, use the one with the largest imaginary part
                iMode = np.argmax(self.M.wmode__DQy.imag)
            DQ_mCoh = self.M.wmode__DQy[iMode]
            DQ_mLD      = self.M.wmodeLDDQy[iMode]
            taylorAlpha = self.M.taylorAlphay[iMode]
            if flag_RecalcModes or abs(DQ_mLD)==0:
                Q0   = self.M.Q.Q0y
                detuning = self.M.Qy
                distribution = self.calcInterpDist(self.psi0,plane=1,flag_BC_gradient_0=True)
                DQ_mLD,DQ_mFree,DQ_mSD,cnt,taylorAlpha, relerr = self.SD_calcGrowthRateOneModeOnePsi(Q0,DQ_mCoh,distribution,detuning,**kwargs)

            latEst,latQuad,L_L = PyRADISE.L2D2_functions.analyticalLatency(
                            DQ_mCoh = DQ_mCoh,DQ_mLD=DQ_mLD,taylorAlpha=taylorAlpha,
                            ax = self.M.Q.ay,bx = self.M.Q.by,
                            eta0=self.M.wmodeMom0y[iMode],sigma_k0=self.M.N.sigma_k0y,
                            eta1=self.M.wmodeMom1y[iMode],sigma_k1=self.M.N.sigma_k1y,Trev = 1/self.M.f_rev)

            print("solver_analyticalLatency: Analytical vertical Latency estimate = %.3e = %.3e * %.3f"%(latEst,L_L,latEst/L_L))
            self.latEsty = latEst
            print("solver_analyticalLatency: Analytical vertical Latency quadintg = %.3e = %.3e * %.3f"%(latQuad,L_L,latQuad/L_L))
            self.latQuady = latQuad
            print("                          (for mode nr %d: DQ= %.3e + %.3ej)"%(iMode,DQ_mCoh.real,DQ_mCoh.imag))

        return

    ###################
    ## Solve the PDE ##
    def _dpsidtFV_const(self,t,psi):
        return self.FV_M.dot(psi) + self.BC_V

    def _dpsidtFV_tdep(self,t,psi):
        """
        DEPRECATED - NEVER USE THIS ONE BECAUSE OF THE SLOW CALCULATION WITH PySSD
        """
        # Not relevant time dependence of dQAvg...
        self.M.dQxAvg = self.M.Q.dQx(1,1)

        self.solver_calcModesAndSD(psi,flag_calcSD = self.flag_storeSDWhileSolving)
        flag_Unstable = self.solver_checkStability(iTime=-1)
        if flag_Unstable:
            print('_dpsidtFV_tdep: Beam unstable - exiting')
            sys.exit()
        self.solver_calcCoeff()

        return self.FV_M.dot(psi) + self.BC_V

    def solve(self, **kwargs):
        """
            kwargs include: tsODE, solve_method,debug ,flag_FindAlpha
            WARNING: Giving these kwargs will modify the corresponding parameters of the object, not just in this function!
        """
        start = time.time()

        ## Load keyword arguments
        if 'tsODE' in kwargs:
            tsODE = kwargs.pop('tsODE',self.tsODE)
            self.setTime(tsODE)
        self.debug = kwargs.pop('debug',self.debug)
        self.solve_method   = kwargs.pop('solve_method',self.solve_method)
        self.flag_FindAlpha = kwargs.get('flag_FindAlpha',self.flag_FindAlpha)
        flag_UpdateCoeff= self.flag_UpdateCoeff = kwargs.get('flag_UpdateCoeff',self.flag_UpdateCoeff)
        self.flag_AdjustTmax  = kwargs.get('flag_AdjustTmax',self.flag_AdjustTmax)

        self.latEstx = self.latQuadx = self.latNumx = np.nan
        self.latEsty = self.latQuady = self.latNumy = np.nan

        ## 1: Calculate Stability diagram and Landau damped modes
        ## 2: Check stability
        ## 3: Calculate coefficients and matrices
        self.solver_init()
        ## 4: Initialize solve_ivp
        dpsidt=lambda t, y: self._dpsidtFV_const(t, y)
        if self.solve_method=='LSODA':
            jac = lambda t, y: self.FV_M.toarray()
            print('WARNING in solve: LSODA solver does not support sparse matrices in this PyRADISE - Calculation requires more memory.')
        else:
            jac = lambda t, y: self.FV_M

        ## Adjust tmax
        if self.flag_AdjustTmax:
            print("solve: Trying to adjust tmax to better fit to a numerical latency estimate.\n",
                  "       Set tmax to 2*tmax_estimate.")

            newTmax = self.tmax

            ## Iterate until the change of estimated time length is smaller than a tolerance (hardcoded at 10%)
            while True:
                oldTmax = newTmax
                testTmax = oldTmax*0.1  #[1/2,.1][flag_UpdateCoeff]

                ## 5: Solve for distribution evolution
                solution = integrate.solve_ivp(dpsidt, t_span=[self.tsODE[0],testTmax],y0=self.psi0,
                                          t_eval=[self.tsODE[0],testTmax],
                                          method=self.solve_method,
                                          jac=jac,
                                          vectorized=True
                                      )
                psi_test = solution.y.T

                ## X: Check stability of the last distribution
                self.solver_calcModesAndSD(psi_test[1],flag_calcSD = False,**kwargs)
                self.solver_checkStability(iTime=1,**kwargs)

                maxRelChange_testx=maxRelChange_testy=-np.inf
                if self.M.nWmodex>0:
                    margin0 = self.stabilityMargins0x[self.indLeastStablex[1]]
                    maxRelChange_testx = -(self.stabilityMarginx[1]-margin0)/margin0
                if self.M.nWmodey>0:
                    margin0 = self.stabilityMargins0y[self.indLeastStabley[1]]
                    maxRelChange_testy = -(self.stabilityMarginy[1]-margin0)/margin0


                ## X: Find new tmax
                maxRelChange_test=max(maxRelChange_testx,maxRelChange_testy)
                if maxRelChange_test<0:
                    print(  'WARNING in solve:  When adjusting for tmax, it was not found that an instability was approached',
                            '                   Therefore, the given tmax will be used.',
                            '                   If it is believed that an instability should be approached, try increasing:'
                            '                       Ncx, Ncy, nQ')
                    if self.debug>1:
                        self.solver_storeSD(1)
                        self.SD_plotAllSD(plane=0,flag_FillSD=0,flag_PlotModes=1)
                        plt.show()
                        print('DEBUG in solve: Press enter to continue running.')
                        input()
                    break

                newTmax = self.tsODE[0]+(testTmax-self.tsODE[0])/maxRelChange_test   *[1,1/4][flag_UpdateCoeff] * 2
                # ...] *2 for margin of estimate,
                # ,.../4] for assumption of cubic behaviour if flag_UpdateCoeff
                printstring=''
                if ( (self.ntODE<100 or
                     (self.iCoeff in [3,4] and not(self.flag_UpdateAlpha and np.any(self.flag_FindAlpha)) ) )
                     and flag_UpdateCoeff):
                    newTmax *= 2
                    printstring=' (added factor 2)'
                    # ,2*...] for more accurate-ish estimate if flag_UpdateCoeff but few time steps
                    #TODO: Introduce a parameter scale_AdjustTmax so that one can define the extra margin from the input parameters
                print('solve: Changed tmax=%.2e s -> %.2e s %s'%(oldTmax,newTmax,printstring))

                if (oldTmax/newTmax) < 1.1 and (newTmax/oldTmax) < 1.1:
                    ## X: Update the times
                    tsODE = self.tsODE*newTmax/self.tmax
                    self.setTime(tsODE)
                    break

        ## Do time iteration
        print('\nsolve: Solve the diffusion equation.')
        if not flag_UpdateCoeff :

            ## 5: Solve for distribution evolution
            solution = integrate.solve_ivp(dpsidt, t_span=[0,self.tmax],y0=self.psi0,
                                            t_eval=self.tsODE,
                                            method=self.solve_method,
                                            jac=jac,
                                            vectorized=True
                                        )
            ts = solution.t
            if not np.all(ts==self.tsODE):
                print('WARNING in solve: Not same input and output times. Biggest offset is %.0e'%(
                        np.max(np.abs(self.tsODE-ts))))
            self.psis=solution.y.T

            ## 6: Reduce the number of stored distributions
            self.setTime(self.tsODE)
            self.psis = self.psis[self.indexPSI]

            ## 7: Check stability of the last distribution
            self.solver_calcModesAndSD(self.psis[-1],flag_calcSD = False,**kwargs)
            flag_unstable = self.solver_checkStability(iTime=-1,**kwargs)
            if flag_unstable:
                print('solve: The final distribution is unstable.')

            ## DEPRECATED: Calculate growthrates of the first mode
            # if self.M.nWmodex>0 and self.M.flag_Wmode: self.SD_calcGrowthRate(plane=0,DQ_mCoh = self.M.wmode__DQx[0])
            # if self.M.nWmodey>0 and self.M.flag_Wmode: self.SD_calcGrowthRate(plane=1,DQ_mCoh = self.M.wmode__DQy[0])

        elif flag_UpdateCoeff:
            nSteps = self.ntODE
            self.psis = np.zeros((nSteps,np.size(self.psi0)))
            self.psis[0] = self.psi0

            for iTime in range(nSteps):

                ## 1: Calculate Stability diagram and Landau damped modes
                self.solver_calcModesAndSD(self.psis[iTime],flag_calcSD = self.flag_storeSDWhileSolving,**kwargs)
                if self.flag_storeSDWhileSolving:
                    self.solver_storeSD(iTime)

                ## 2: Check stability
                if iTime > 0:
                    flag_Unstable = self.solver_checkStability(iTime,**kwargs)

                    if flag_Unstable:
                        print('\nsolve: Stopping the calculation - An instability was reached after %d steps'%iTime)

                        ## Delete empty data preallocated for later times
                        nt=iTime+1
                        self.ntODE = nt
                        self.tsODE = self.tsODE[:nt]

                        if self.M.nWmodex>0:
                            self.indLeastStablex  = self.indLeastStablex[:nt]
                            self.growthRatex      = self.growthRatex[:nt]
                            self.stabilityMarginx = self.stabilityMarginx[:nt]
                        if self.M.nWmodey>0:
                            self.indLeastStabley  = self.indLeastStabley[:nt]
                            self.growthRatey      = self.growthRatey[:nt]
                            self.stabilityMarginy = self.stabilityMarginy[:nt]

                        ## Report Latency
                        for plane in [0,1]:
                            if [self.M.nWmodex,self.M.nWmodey][plane]==0:
                                continue
                            if   plane==0: stabilityMargin = self.stabilityMarginx - self.relInstabilityThreshold*self.stabilityMargins0x[self.indLeastStablex[-1]]
                            elif plane==1: stabilityMargin = self.stabilityMarginy - self.relInstabilityThreshold*self.stabilityMargins0y[self.indLeastStabley[-1]]

                            ind = np.argmax(stabilityMargin<0)-1
                            if ind>=0:
                                lat = self.tsODE[ind]-stabilityMargin[ind]/(stabilityMargin[ind+1]-stabilityMargin[ind])*(self.tsODE[ind+1]-self.tsODE[ind])
                                if lat > self.time_scale*1e-1:
                                    print('solve: Numerical  %s Latency = %.2f %s = %.6e'%(['Horizontal','Vertical'][plane],lat/self.time_scale,self.tunit,lat))
                                else:
                                    print('solve: Numerical  %s Latency = %.2e %s = %.6e'%(['Horizontal','Vertical'][plane],lat/self.time_scale,self.tunit,lat))

                                if np.isfinite(self.latQuadx) and plane==0:
                                    print("solve: Analytical Horizontal Latency = %.3es"%(self.latQuadx))
                                if np.isfinite(self.latQuady) and plane==1:
                                    print("solve: Analytical Vertical Latency = %.3es"%(self.latQuady))

                                if plane==0:
                                    self.latNumx=lat
                                else:
                                    self.latNumy=lat
                        break
                printstring = ''
                if self.M.nWmodex+self.M.nWmodey>0: printstring+='stabilityMargin(t=%.2es):'%self.tsODE[iTime]
                if self.M.nWmodex>0: printstring+=' SMx=%.2e'%self.stabilityMarginx[iTime]
                if self.M.nWmodey>0: printstring+=' SMy=%.2e'%self.stabilityMarginy[iTime]

                if iTime==nSteps-1:
                    ## Only iterate N-1 steps to get N times (including initial condition)
                    print('solve: Final timestep done %d: %.2es -> %.2es | %s'%(iTime-1,self.tsODE[iTime-1],self.tsODE[iTime],printstring))
                    break

                ## 3: Recalculate coefficients and matrices
                flag_RecalcCoeff = True
                if self.iCoeff in [5,6] and self.M.nWmodex>0:
                    margin0 = self.stabilityMargins0x[self.indLeastStablex[iTime]]
                    if self.stabilityMarginx[iTime]/margin0 < 0.2: flag_RecalcCoeff = self.flag_UpdateCoeffAllTheWay
                if self.iCoeff in [5,6] and self.M.nWmodey>0:
                    margin0 = self.stabilityMargins0y[self.indLeastStabley[iTime]]
                    if self.stabilityMarginy[iTime]/margin0 < 0.2: flag_RecalcCoeff = self.flag_UpdateCoeffAllTheWay

                if flag_RecalcCoeff:
                    if self.iCoeff in [5,6] and self.flag_SDRemoveLoops:
                        self.M.DQ_SDx = self.SD_removeLoops(self.M.DQ_SDx)
                        self.M.DQ_SDy = self.SD_removeLoops(self.M.DQ_SDy)
                    self.solver_calcCoeff(**kwargs)

                ## 4: Initialize solve_ivp
                dpsidt=lambda t, y: self._dpsidtFV_const(t, y)
                if self.solve_method=='LSODA':
                    jac = lambda t, y: self.FV_M.toarray()
                    print('WARNING in solve: LSODA solver does not support sparse matrices in this PyRADISE - Calculation requires more memory.')
                else:
                    jac = lambda t, y: self.FV_M

                ## 5: Solve for distribution evolution - ONE TIME STEP
                print('solve: Starting timestep %d: %.2es -> %.2es | %s'%(iTime,self.tsODE[iTime],self.tsODE[iTime+1],printstring))
                solution = integrate.solve_ivp(dpsidt, t_span=[self.tsODE[iTime],self.tsODE[iTime+1]],y0=self.psis[iTime],
                                              t_eval=[self.tsODE[iTime+1]],
                                              method=self.solve_method,
                                          #   method='RK45',
                                              jac=jac,
                                              vectorized=True
                                          )
                self.psis[iTime+1] = solution.y.T[0]

                ## Need to invoke the garbage collector to avoid memory leakage
                ## Known "bug" of repeated use of integrate.solve_ivp
                gc.collect()

            ## End for loop - Done solving the distribution evolution

            ## 6: Reduce the number of stored distributions
            self.setTime(self.tsODE)
            self.psis = self.psis[self.indexPSI]
            if self.flag_storeSDWhileSolving:
                self.DQ_SDAllx = self.DQ_SDAllx[self.indexSD]
                self.Q_freeAllx= self.Q_freeAllx[self.indexSD]
                self.DQ_SDAlly = self.DQ_SDAlly[self.indexSD]
                self.Q_freeAlly= self.Q_freeAlly[self.indexSD]

        tot = time.time()-start
        print('solve: Solving the PDE took a total wall time of %.2fs\n'%(tot))

        ## Delete PDE solver matrices to reduce memory consumption
        del(self.FV_M)
        del(self.BC_V)
        self.G.grid_releaseMemory()

        return self.psis


    ###########
    ##
