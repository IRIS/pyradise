import numpy as np
from scipy import integrate
import scipy.constants as const
import sys

sys.path.append('../') # To get PySSD
from PySSD.Integrator import Integrator
from PySSD.Distribution import Distribution,Gaussian
from PySSD.Detuning import LinearDetuning

import PyRADISE

def analyticalLatency(DQ_mCoh,DQ_mLD,taylorAlpha,ax,bx,
                eta0,sigma_k0,eta1=0,sigma_k1=0,Trev = 1/11245.5):
    if ax==0:
        return np.inf, np.inf, np.inf

    sum_sigma2eta2 = sigma_k0**2*eta0**2 + sigma_k1**2*eta1**2

    BoBmA = bx/(bx-ax)
    Jxmin = max(0,DQ_mLD.real/ax)
    Jxeff = Jxmin +BoBmA + BoBmA**2/(Jxmin+BoBmA)
    DQ_mSD = DQ_mCoh - 1j*DQ_mLD.imag/taylorAlpha.real

    L_L = (  Trev/2.5 * -(DQ_mLD.imag)**5/(taylorAlpha.real*DQ_mSD.imag *ax**2* abs(DQ_mCoh)**2)
           *1/(Jxeff*sum_sigma2eta2)  )

    tempfunc = lambda x: -5*x**4/(1+(1-x)*DQ_mLD.imag/(DQ_mSD.imag*taylorAlpha.real))
    latQuad = integrate.quad(tempfunc,1,0)[0] *L_L

    latEst = L_L*(1+np.imag(DQ_mSD-DQ_mCoh)/(6*DQ_mSD.imag))
    return latEst,latQuad,L_L


def getDQSD(DQ_mCoh,DQ_mLD,taylorAlpha):
    ## DQ_mSD=BTF(DQ_mFree)
    ## DQ_mCoh=BTF(DQ_mLD)
    DQ_mSD =   DQ_mCoh -1j*DQ_mLD.imag/taylorAlpha.real
    DQ_mFree = np.real(DQ_mLD + taylorAlpha*(DQ_mSD-DQ_mCoh))
    return DQ_mFree, DQ_mSD


def calcCapitalOmega(distribution,detuning,integrator_epsilon,Q0,DQ_mCoh,
                        tol=1e-4,flag_FindAlpha=[1,1],taylorAlphaOld=0+0j,debug=0,JMaxSD=18,nStep=2000):

    ## Set the integrator - Only set 1 to reduce memory usage
    integrator1 = Integrator(distribution, detuning, maxJ=JMaxSD,epsilon=integrator_epsilon*1,nStep=nStep)

    DQ_mLD,DQ_mFree,DQ_mSD,cnt,taylorAlpha,relerr = PyRADISE.Coefficients.calc_LandauDampedOneMode(integrator_epsilon,integrator1,Q0,DQ_mCoh,
                                   tol=tol,flag_FindAlpha=flag_FindAlpha,taylorAlphaOld=taylorAlphaOld,debug=debug)
    # DQ_mFree, DQ_mSD = getDQSD(DQ_mCoh,DQ_mLD,taylorAlpha)

    return DQ_mLD , cnt, taylorAlpha, relerr, DQ_mFree, DQ_mSD


#########
## Find threshold and constant analyticalLatency
def searchGoalNewton(flag_TooLarge,val,minTooLarge,maxTooSmall,err=0,errTooLarge=1,errTooSmall=-1):

    if flag_TooLarge:
        minTooLarge = val
        errTooLarge = err
    else:
        maxTooSmall = val
        errTooSmall = err

    if np.isfinite(minTooLarge):
        if errTooLarge==1 or errTooSmall==-1:
            val = (maxTooSmall + minTooLarge)/2
        else:
            val = maxTooSmall + (minTooLarge-maxTooSmall)*(0-errTooSmall)/(errTooLarge-errTooSmall)
    else:
        val = maxTooSmall*2
    return val,minTooLarge,maxTooSmall,errTooLarge,errTooSmall

def searchGoalBinary(flag_TooLarge,val,minTooLarge,maxTooSmall):
    return searchGoalNewton(flag_TooLarge,val,minTooLarge,maxTooSmall)[:3]

def find_axThreshold(DQ_mCoh,Qx,asign=1,bafac=-0.7,distribution=Gaussian(),JMaxSD=18,nStep=2000,
                     method=1,tol_search=1e-3,goal=0,
                     tol=1e-4,flag_FindAlpha=[1,1],debug=0):
    if DQ_mCoh.imag<=0:
        print('find_axThreshold: Mode is stable with non-positive imaginary part')
        return 0,DQ_mCoh,1,0

    ax =  asign*np.abs(DQ_mCoh)
    tol_scale=DQ_mCoh.imag

    minTooLarge=np.inf
    maxTooSmall=0
    errTooLarge=1 ; errTooSmall=-1
    errClosest=np.inf
    cnt=0
    while True:
        if bafac>1e6:
            bx=ax
            ax=0
        else:
            bx=bafac*ax

        integrator_epsilon = 1e-2 * max(abs(ax),abs(bx))  *[1,2][1*(np.abs((bx**2+ax**2)/(bx*ax+1e-16))>10)]
        detuning = LinearDetuning(Qx,ax,bx)


        DQ_mLD,cnt_DQ_mLD,taylorAlpha,relerr,_,_=calcCapitalOmega(distribution,detuning,integrator_epsilon,
                                                  Qx,DQ_mCoh,tol=tol,flag_FindAlpha=flag_FindAlpha,
                                                  debug=-1,JMaxSD=JMaxSD,nStep=nStep)

        err = (DQ_mLD.imag-goal)/tol_scale

        if debug>0:
            print('find_axThreshold: step %-2d: DQ_mLD = %.2e %s%.3ej with ax=%.2e in [%.2e,%.2e], bx=%.2e - err=%.1e'%(
                cnt,DQ_mLD.real,['+','-'][np.sign(DQ_mLD.imag)<0],abs(DQ_mLD.imag),ax,maxTooSmall,minTooLarge,bx,err))

        # If use no in-plane term
        if bafac>1e6:
            ax=bx

        # Store best value
        if abs(err/errClosest)<1:
            errClosest=err
            valClosest=[ax,DQ_mLD,taylorAlpha]

        cnt+=1
        if abs(err) < tol_search or cnt>20:
            break

        flag_TooLarge = err < 0
        if method==1:
            ax,minTooLarge,maxTooSmall = searchGoalBinary(flag_TooLarge,ax,minTooLarge,maxTooSmall)
        elif method==2:
            ax,minTooLarge,maxTooSmall,errTooLarge,errTooSmall = searchGoalNewton(flag_TooLarge,ax,minTooLarge,maxTooSmall,err,errTooLarge,errTooSmall)

    ax,DQ_mLD,taylorAlpha = valClosest
    return ax,DQ_mLD,taylorAlpha,cnt

def find_axLatency(DQ_mCoh,Qx,asign=1,bafac=-0.7,distribution=Gaussian(),JMaxSD=18,
                   eta0=1,sigma_k0=1e-4,eta1=0,sigma_k1=0,Trev = 1/11245.5,
                     method=1,tol_search=1e-3,goal=1,
                   tol=1e-4,flag_FindAlpha=[1,1],debug=0):
    if DQ_mCoh.imag<=0:
        print('find_axLatency: Mode is stable with non-positive imaginary part')
        return 0,DQ_mCoh,1,np.inf,0
    ax = asign*abs(DQ_mCoh)

    sum_sigma2eta2 = sigma_k0**2*eta0**2 + sigma_k1**2*eta1**2

    tol_scale=[goal,Trev/(sum_sigma2eta2*100)][goal==0]

    minTooLarge=np.inf
    maxTooSmall=0
    errTooLarge=1 ; errTooSmall=-1
    errClosest = np.inf
    cnt=0
    while True:
        bx = bafac*ax

        integrator_epsilon = 1e-2 * max(abs(ax),abs(bx))  *[1,2][1*(abs((bx**2+ax**2)/(bx*ax+1e-16))>10)]

        detuning = LinearDetuning(Qx,ax,bx)

        DQ_mLD,cnt_DQ_mLD,taylorAlpha,relerr,_,_=calcCapitalOmega(distribution,detuning,integrator_epsilon,
                                                  Qx,DQ_mCoh,tol=tol,flag_FindAlpha=flag_FindAlpha,
                                                  debug=-1,JMaxSD=JMaxSD,nStep=nStep)
        # if np.imag(DQ_mLD)>0:
        #     err = np.imag(DQ_mLD)/np.imag(DQ_mCoh)
        # else:
        latEst,latQuad,_ = analyticalLatency(DQ_mCoh,DQ_mLD,taylorAlpha,ax,bx,
                                    eta0,sigma_k0,eta1,sigma_k1,Trev=Trev)
        err = (latQuad-goal)/tol_scale

        if debug>0:
            print('find_axLatency: step %-2d: Latency=%.4e!=%.4e with ax=%.2e in [%.2e,%.2e] - err=%.1e'%(
                cnt,latQuad,goal,ax,maxTooSmall,minTooLarge,err))

        # Store best value
        if abs(err/errClosest)<1:
            errClosest=err
            valClosest=[ax,DQ_mLD,taylorAlpha,latQuad]

        # Iterate
        cnt+=1
        if abs(err) < tol_search or cnt>20:
            break

        flag_TooLarge = err > 0
        if method==1:
            ax,minTooLarge,maxTooSmall = searchGoalBinary(flag_TooLarge,ax,minTooLarge,maxTooSmall)
        elif method==2:
            ax,minTooLarge,maxTooSmall,errTooLarge,errTooSmall = searchGoalNewton(flag_TooLarge,ax,minTooLarge,maxTooSmall,err,errTooLarge,errTooSmall)

    return valClosest[0],valClosest[1],valClosest[2],valClosest[3],cnt


def axToIoct(ax,emittNormx=1e-6,beamNumber=1,energy_MeV=6.5e6):
    print('WARNING in axToIoct: This function converts ax to Ioct only for the horizontal plane in the LHC as of 2018 - approximately.')
    if not beamNumber in [1,2]:
        print('WARNING in axToIoct: beamNumber is either 1 or 2 - setting %d to 1'%beamNumber)
        beamNumber=1
    gamma = energy_MeV/const.value("proton mass energy equivalent in MeV")
    beta = np.sqrt(1-1/gamma**2)
    emittGeox = emittNormx/(beta*gamma)
    Ioct = ax / ([543.2,531.3][beamNumber-1] * emittGeox * 6.5e6/energy_MeV)
    return Ioct
