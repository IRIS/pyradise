# #!/afs/cern.ch/work/s/sfuruset/99_programfiles/anaconda3/bin/python3
import numpy as np
import pickle as pkl
import time
import sys,os

import PyRADISE
from plot_PyRADISE import *
from calc_PyRADISE import calc_init, calc_PSI,calc_SD


"""
DEPRECATED - EVERYTHING THAT COULD BE DONE FROM HERE IS NOW DONE IN calc_PyRADISE.py
"""

# ## Parameters for this file
# flag_TestPlots = 0
# flag_SavePkl = 1
#
# ######################################
# def main():
#     ## Load calc from file
#     if len(sys.argv)>1:
#         filename = sys.argv[1]
#         inFilename,flag_PklExists = checkFilename(filename)
#         if flag_PklExists:
#             print('Load an instance of PyRADISE from file: %s'%inFilename)
#             with open(inFilename+'.pkl','rb') as pklfile:
#                 calc = pkl.load(pklfile)
#
#             # Check not already computed
#             if os.path.isfile(inFilename[:-2]+'2.pkl'):
#                 print('Job already done for %s \nABORT'%inFilename)
#                 sys.exit()
#         else:
#             print('Instance of PyRADISE on file does not exist: %s'%inFilename)
#             sys.exit()
#
#     ## Initialize a fresh calc
#     else:
#         calc,inFilename = calc_init()
#         print('Initialize a new instance of PyRADISE: %s'%inFilename)
#         inFilename,flag_PklExists = checkFilename(inFilename)
#
#     if flag_TestPlots:
#         testPlots_S0(calc)
#         input()
#
#     # If scaled
#     scale = None
#     if len(sys.argv)>2:
#         scale = float(sys.argv[2])
#
#
#     # Calculate
#     # Do not repeat solving the PDE
#     calcName = inFilename[inFilename.rfind('/'):-3]
#     if int(inFilename[-1])<1:
#         calc = calc_PSI(calc,calcName)
#
#     # calc.flag_storeSDWhileSolving = 0
#     # calc.maxCPU=4
#
#     calc_SD(calc,calcName,scale)
#
# if __name__=="__main__":
#     main()
