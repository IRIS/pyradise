import numpy as np
import sys
from argparse import Namespace

sys.path.append('../') # Path to setup_PyRADISE, calc_PyRADISE, and plot_PyRADISE
from setup_PyRADISE import dump_setup, paths, set_integrator_epsilon
from calc_PyRADISE import checkFilename,calc_init,calc_PSI,calc_SD,dump_results
from plot_PyRADISE import testPlots_S2,testPlots_S3

########################################
####### Configuration parameters #######
########################################
s= Namespace()
s.flag_Radial = 1               # true: radial PDE, false: action PDE
s.flag_ICPoint = 0              # IC given as point value or average for cell
s.solve_method=['RK45','RK23','Radau','BDF','LSODA'][3] # Use BDF, if not, it is numerically unstable
s.flag_AdjustTmax  = 0          # Calculate approximately how long the latency will be to set tmax
s.flag_UpdateCoeff = 1          # 1 (0): Recalculate Diffusion coeffiecent every step
s.flag_UpdateCoeffAllTheWay = 1 # If False (and flag_UpdateCoeff is True): Do not update coefficients the last X% of the drilling (see Solver.py)
s.flag_SDRemoveLoops = 0        # If True (and flag_UpdateCoeff is True): Remove loops of SD to calculate the diffusion coefficient
s.relInstabilityThreshold = 0.0 # (If flag_UpdateCoeff is True): Consider instability reached when relInstabilityThreshold*100% remains of the drilling
s.flag_IncludeDirectNoise = 0   # If True, model diffusion driven directly by the noise and by the wake. If false, only wake noise.
s.flag_ProjX = 0                # true: Calculate projection on x and y axis (time consuming and rarely interesting)
s.xMax_ProjX = 3                # Maximum position amplitude to project to
s.flag_AdjustGrid = 0           # true: shift grid so that one cell is centered on the avg. Interesting in 1D with iCoeff=2
s.maxCPU = 1                    # Maximum number of CPUs to use for SD calculation
s.BC_xMax = 0                   # i = [Dirichlet Psi=0, Neumann djPsi=0, Robin: djPsi+Psi=0 or drPsi+rPsi=0, dPsi const]
s.BC_xVal = 0                   # Value of BC at xMax
s.BC_yMax = s.BC_xMax
s.BC_yVal = s.BC_xVal
s.debug   = 0                   # If>0: Print additional information to user


## Define time steps
s.time_scale=1                  # To get prefactor on time, [0.001,1,60,3600,'auto']
s.f_rev = 11.2455e3             # Revolution frequency (relevant since noise is given in amplitude per turn)
s.tmax = 10                     # Max number of seconds to iterate distribution for
s.nturns = s.tmax/s.f_rev       # Max number of turns ----------------------------
s.ntODE = 11                    # Number of times to solve the distribution for, overruled if flag_AdjustTmax=True
s.tsODE = np.linspace(0,s.tmax,s.ntODE)
s.ntPSImax = 100                # Maximum number of times to save the distribution for


## Define grid
s.ND = 2                        # Number of dimensions, 1 or 2
s.Ncx = 400                     # Number of cells in horizontal action space
s.Ncy = s.Ncx                   # Number of cells in vertical action space
s.JxMax = 20                    # Max horizontal action in the PDE
s.JyMax = s.JxMax               # Max vertical action in the PDE
s.rxMax = np.sqrt(2*s.JxMax)    # Max horizontal phase-space-radius
s.ryMax = np.sqrt(2*s.JyMax)    # Max vertical phase-space-radius
s.JMaxSD = [18, s.JxMax][1]     # Max action to integrate over with PySSD

s.interpOrderPsi = 1            # Interpolation order of the distribution


#######################################
## Default Beam & Machine parameters ##
s.Qx = 0.31                     # Horizontal fractional tune
s.Qy = 0.32                     # Vertical fractional tune
s.Qs = 0.00191                  # Synchrotron tune
s.sigma_dpp = 1e-4              # rms momentum spread
s.Qpx= 15                       # Horizontal linear chromaticity Q'_x
s.Qpy= 0                        # Vertical linear chromaticity Q'_y
s.gx = 0.01                     # Horizontal feedback gain, damping time = 2/gx
s.gy = s.gx                     # Vertical feedback gain, damping time = 2/gx
s.ax = 1e-5 * 7.5               # Horizontal normalized in-plane detuning coefficient
s.bx = -0.7*s.ax  * 1           # Horizontal normalized cross-plane detuning coefficient
s.ay = s.ax                     # Vertical normalized in-plane detuning coefficient
s.by = s.bx                     # Horizontal normalized cross-plane detuning coefficient


## Physics parameters
s.iCoeff = 3                    # Which diffusion coefficient [0, IBS, IBS + LL (r),NEW, NEW+LL ...,[10],constant (J),K*LL(J)]
                                # iCoeff=1: Intra-beam scattering (IBS)
                                # iCoeff=2: IBS + Feedback affected diffusion (FB)
                                # iCoeff=3: IBS + Wakefield driven diffusion (W-USHO)
                                # iCoeff=4: IBS + FB + W-USHO
                                # iCoeff=5: IBS + Wakefield driven diffusion (W-DI)
                                # iCoeff=6: IBS + FB + W-DI ... NOT IMPLEMENTED, because W-DI actually includes FB if using modes from BimBim

## iCoeff 2: Feedback affected diffusion parameters
s.damperAlpha = 1               # damperAlpha = 0: includes drift, damperAlpha=1: cancel drift term
s.JAvg  = 1                     # Average action of a beam. 1 for exponential beam
s.dQxAvg=(s.ax+s.bx*(s.ND>1))*s.JAvg  # Average Horizontal tune
s.dQyAvg=(s.ay+s.by*(s.ND>1))*s.JAvg  # Average Vertical tune

## iCoeff 3,5: Noise Excited Wakefield parameters
s.wmodeQ0x=[]   ; s.wmode__DQx=[]   ; s.wmodeMom0x=[]  ;  s.wmodeMom1x=[]    # Initialized to empty
s.wmodeQ0y=[]   ; s.wmode__DQy=[]   ; s.wmodeMom0y=[]  ;  s.wmodeMom1y=[]    # Initialized to empty
s.wmodeQ0x   = [s.Qx]  ; s.wmode__DQx = [-1.47e-4 + 1.25e-5j]  ; s.wmodeMom0x = [1] ;  s.wmodeMom1x = [1]


## iCoeff 3: Noise Excited Wakefield parameters - USHO approach
s.flag_FindAlpha = [1,1]        # update [real,imaginary] part of taylorAlpha (iCoeff=3)
s.flag_UpdateAlpha = 1          # Update taylorAlpha at each timestep or not (iCoeff=3)
s.flag_UpdateReQ = 1            # 1 (0): Update Real(DQ_mLD) every step or not (iCoeff=3)


######################
## Noise parameters ##

## IBS noise
s.sigma_ibsx = 0
s.sigma_ibsy = 0

##  White noise
s.sigma_k0x = 1e-4 * 1
s.sigma_k0y = s.sigma_k0x * 1

## CC amplitude noise
s.sigma_k1x = 1e-4 * 0
s.sigma_k1y = s.sigma_k1x * 0

##################################################
## Parameters for Stability diagram Calculation ##
s.nQ = 200                                  # Number of tunes to be calculated with the Dispersion integral
s.nStep = 2000                              # Grid resolution for PySSD
s.interpOrderSD = 1                         # Interpolation order of the stability diagram
s.widthRatioSD = 0.1                        # s.nQ//2 of the free tunes are in a part widthRatioSD of the total range around the most unstable mode
s.flag_center_tune_is_LDQ = 0               # Calculate the center_tune by estimating the Landau damped tune of the least stable mode - not necessary and is time consuming
s.ntSDmax = s.ntPSImax                      # Max number of PSI to calculate SD for after solving
s.integrator_epsilon_scale = 2
s.integrator_epsilon = set_integrator_epsilon(s.ax,s.bx) * s.integrator_epsilon_scale

s.nSD_scaled = 0                            # How many scaled detuning strengths to calc SD for, relevant for iCoeff=2 (without wakefield modes)
s.scales = []                               # Which scales


######################################
######## Plotting parameters #########
s.plot_rmax    = 6        #
s.flag_Fill    = 0        #

###################################
## Setting the name of the study ##
s.name = 'testcase1'

if __name__=="__main__":
    print(  '\nWELCOME MESSAGE TO THE NEW USER OF PyRADISE:',
            '\n  This test case simulates the same situation as case 1 in https://doi.org/10.1103/PhysRevAccelBeams.23.114401, \n  but with a different discretization:',
            '\n    Ncx=%d (700 in the paper)'%s.Ncx,
            '\n    Ncx=%d (700 in the paper)'%s.Ncy,
            '\n    ntODE=%d (~600 in the paper. However, the time discretization depends on tmax as well.)'%s.ntODE,
            '\n  This calculation will take a couple of minutes.\n')

    calc = calc_init(s=s,flag_SavePkl=0)
    calc = calc_PSI(calc,s.name,flag_SavePkl=0)
    calc = calc_SD(calc,s.name,flag_SavePkl=0)  ;   testPlots_S2(calc)
    calc2= dump_results(calc,s=s,flag_SavePkl=0)


    print(  '\nWELCOME MESSAGE TO THE NEW USER OF PyRADISE:',
            '\n  Note that the numerical horizontal latency (%.2f s) found here is longer than the one found in the paper (1.65 s).'%calc.latNumx,
            '\n  If nothing was changed, the numerical horizontal latency found here should be 4.57 s.'
            '\n  Try increasing ntODE (gradually) to 1000 and Ncx=Ncy to 700 or beyond.',
            '\n  PRO TIP: To get the numerical latency, you do not need to calculate all the stability diagrams! Comment out the line that calls calc_SD.')

    input() # To show plots before they close.
